package patmat

import patmat.Huffman._

object huffmanTree {
  println("Welcome to the Scala worksheet - Huffman Tree")
                                                  //> Welcome to the Scala worksheet - Huffman Tree

  val listOfPairs: List[(Char, Int)] = List(('a', 2), ('b', 1), ('e', 7))
                                                  //> listOfPairs  : List[(Char, Int)] = List((a,2), (b,1), (e,7))
  listOfPairs.mkString(";")                       //> res0: String = (a,2);(b,1);(e,7)

  val pair: (Char, Int) = ('p', 5)                //> pair  : (Char, Int) = (p,5)

  //def hasChar(pair: (Char, Int)) : Boolean =
  //def hasChar = (pair: (Char, Int), c: Char) => pair._1 == c

  def hasChar(c: Char): ((Char, Int)) => Boolean = {
    (p: (Char, Int)) => p._1 == c
  }                                               //> hasChar: (c: Char)((Char, Int)) => Boolean

  listOfPairs.takeWhile((pair: (Char, Int)) => pair._1 == 'a')
                                                  //> res1: List[(Char, Int)] = List((a,2))
  listOfPairs.takeWhile(hasChar('c'))             //> res2: List[(Char, Int)] = List()
  val pairE = listOfPairs.find(hasChar('e'))      //> pairE  : Option[(Char, Int)] = Some((e,7))
  pairE.get._1                                    //> res3: Char = e
  pairE.get._2                                    //> res4: Int = 7
  listOfPairs.exists(hasChar('e'))                //> res5: Boolean = true
  listOfPairs.exists(hasChar('f'))                //> res6: Boolean = false
  listOfPairs.dropWhile(hasChar('a'))             //> res7: List[(Char, Int)] = List((b,1), (e,7))

  val ct1: CodeTree = makeCodeTree(Leaf('a', 7), Leaf('b', 4))
                                                  //> ct1  : patmat.Huffman.CodeTree = Fork(Leaf(a,7),Leaf(b,4),List(a, b),11)
  val ct2: CodeTree = makeCodeTree(makeCodeTree(Leaf('c', 1), Leaf('d', 3)), Leaf('e', 5))
                                                  //> ct2  : patmat.Huffman.CodeTree = Fork(Fork(Leaf(c,1),Leaf(d,3),List(c, d),4)
                                                  //| ,Leaf(e,5),List(c, d, e),9)
  val ct3: CodeTree = makeCodeTree(makeCodeTree(Leaf('f', 3), Leaf('g', 6)), makeCodeTree(Leaf('h', 3), Leaf('i', 2)))
                                                  //> ct3  : patmat.Huffman.CodeTree = Fork(Fork(Leaf(f,3),Leaf(g,6),List(f, g),9
                                                  //| ),Fork(Leaf(h,3),Leaf(i,2),List(h, i),5),List(f, g, h, i),14)
   
  val ctl0: List[CodeTree] = List()               //> ctl0  : List[patmat.Huffman.CodeTree] = List()
  val ctl1 = ct1 :: Nil                           //> ctl1  : List[patmat.Huffman.CodeTree] = List(Fork(Leaf(a,7),Leaf(b,4),List(
                                                  //| a, b),11))
  val ctl2 = ct1 :: ct2 :: Nil                    //> ctl2  : List[patmat.Huffman.CodeTree] = List(Fork(Leaf(a,7),Leaf(b,4),List(
                                                  //| a, b),11), Fork(Fork(Leaf(c,1),Leaf(d,3),List(c, d),4),Leaf(e,5),List(c, d,
                                                  //|  e),9))
  val ctl3 = List(ct1, ct2, ct3)                  //> ctl3  : List[patmat.Huffman.CodeTree] = List(Fork(Leaf(a,7),Leaf(b,4),List(
                                                  //| a, b),11), Fork(Fork(Leaf(c,1),Leaf(d,3),List(c, d),4),Leaf(e,5),List(c, d,
                                                  //|  e),9), Fork(Fork(Leaf(f,3),Leaf(g,6),List(f, g),9),Fork(Leaf(h,3),Leaf(i,2
                                                  //| ),List(h, i),5),List(f, g, h, i),14))

  val l1 = List(1,2,3,4)                          //> l1  : List[Int] = List(1, 2, 3, 4)
  val l2 = l1 ++ List(5)                          //> l2  : List[Int] = List(1, 2, 3, 4, 5)
 
}