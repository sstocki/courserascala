package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {

  trait TestBuildingTrees {

    val frenchLetters: List[(Char, Int)] = List(('s', 121895), ('d', 56269), ('x', 5928), ('j', 8351), ('f', 16351),
      ('z', 2093), ('k', 745), ('w', 1747), ('y', 4725), ('h', 11298), ('q', 20889), ('o', 82762), ('l', 83668),
      ('m', 45521), ('p', 46335), ('u', 96785), ('r', 100500), ('c', 50003), ('v', 24975), ('g', 13288), ('b', 13822),
      ('n', 108812), ('t', 111103), ('e', 225947), ('i', 115465), ('a', 117110))

    // tree form the example
    val exampleLetters: List[(Char, Int)] = List(('a', 8), ('b', 3), ('c', 1), ('d', 1), ('e', 1), ('f', 1), ('g', 1), ('h', 1))

    def huffmanString(letters: List[(Char, Int)]): String = {

      def iter(letters: List[(Char, Int)], s: String): String = {
        if (letters.isEmpty) s
        else {
          iter(letters.tail, (letters.head._1.toString() * letters.head._2) + s)
        }
      }

      iter(letters, "")
    }

    val frenchString = huffmanString(frenchLetters)
    val exampleString = huffmanString(exampleLetters)

    // val exampleBits: List[Bit] = List(1, 0, 0, 0, 1, 0, 1, 0) // original example value
    // my tree is different that in the example, huffman trees are not unique
    val exampleBits: List[Bit] = List(1, 1, 1, 0, 1, 0, 0, 1)
    val exampleBitsDecoded: List[Char] = List('b', 'a', 'c')

    //    println("exampleBits = " + exampleBits.mkString)
    //    println("exampleBitsDecoded = " + exampleBitsDecoded)

  }

  trait TestTrees {
    val tx = Leaf('x', 21)
    val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
    val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)

    val ct1a: CodeTree = makeCodeTree(Leaf('a', 7), Leaf('b', 4)) // w == 11
    val ct1b: CodeTree = makeCodeTree(Leaf('w', 6), Leaf('x', 6)) // w == 12
    val ct1c: CodeTree = makeCodeTree(Leaf('y', 6), Leaf('z', 7)) // w == 13
    val ct2: CodeTree = makeCodeTree(makeCodeTree(Leaf('c', 3), Leaf('d', 4)), Leaf('e', 7)) // w == 14
    val ct3: CodeTree = makeCodeTree(makeCodeTree(Leaf('f', 3), Leaf('g', 6)), makeCodeTree(Leaf('h', 3), Leaf('i', 3)))
    val ct1ab: CodeTree = makeCodeTree(ct1a, ct1b) // w = 11 + 12 = 23
    val ct1c2: CodeTree = makeCodeTree(ct1c, ct2) // w = 13 + 14 = 27
    val ct31ab: CodeTree = makeCodeTree(ct3, ct1ab) // w = 23 + 15 = 38
    val ct1c231ab: CodeTree = makeCodeTree(ct1c2, ct31ab) // w = 65

    val charList1 = List('a', 'b', 'a', 'c', 'a', 'x', 'x', 'z')

    val ctl0: List[CodeTree] = List()
    val ctl1 = ct1a :: Nil
    val ctl2 = ct1b :: ct2 :: Nil
    val ctl3 = List(ct1a, ct2, ct3)
    val ctl5 = List(ct1a, ct1b, ct1c, ct2, ct3)

    val ctlExample = List(Leaf('c', 1), Leaf('d', 1), Leaf('e', 1), Leaf('f', 1), Leaf('g', 1), Leaf('h', 1),
      Leaf('b', 3), Leaf('a', 8))

    val decodedSecretExample = "huffmanestcool"
  }

  test("weight of t1") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("weight of t2") {
    new TestTrees {
      assert(weight(t2) === 9)
    }
  }

  test("weight of tx") {
    new TestTrees {
      assert(weight(tx) === 21)
    }
  }

  test("weight of ct2") {
    new TestTrees {
      assert(weight(ct2) === 14)
    }
  }

  test("chars of t1") {
    new TestTrees {
      assert(chars(t1) === List('a', 'b'))
    }
  }

  test("chars of t2") {
    new TestTrees {
      assert(chars(t2) === List('a', 'b', 'd'))
    }
  }

  test("chars of tx") {
    new TestTrees {
      assert(chars(tx) === List('x'))
    }
  }

  test("times of charList1") {
    new TestTrees {
      val result = times(charList1)
      assert(result.size === 5)
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 3)))
  }

  test("singleton of ctl1") {
    new TestTrees {
      assert(singleton(ctl1) === true)
    }
  }

  test("singleton of ctl3") {
    new TestTrees {
      assert(singleton(ctl3) === false)
    }
  }

  test("singleton empty list") {
    new TestTrees {
      assert(singleton(ctl0) === false)
    }
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4)))
  }

  test("combine of ctl5") {
    new TestTrees {
      assert(combine(ctl5) === List(ct1c, ct2, ct3, ct1ab))
    }
  }

  test("combine of ctl1 - single element list") {
    new TestTrees {
      assert(combine(ctl1) === ctl1)
    }
  }

  test("combine of ctl2 - two element list") {
    new TestTrees {
      assert(combine(ctl2) === List(makeCodeTree(ct1b, ct2)))
    }
  }

  test("combine of empty list") {
    new TestTrees {
      assert(combine(ctl0) === ctl0)
    }
  }

  test("until of ctl5") {
    new TestTrees {
      assert(until(singleton, combine)(ctl5) === List(ct1c231ab))
    }
  }

  test("until of ctlExample") {
    new TestTrees {
      val result = until(singleton, combine)(ctlExample)
      // println("result = " + result)
      //assert(until(singleton, combine)(ctlExample) === List(ct1c231ab))
    }
  }

  test("until of ctl1 - single element list") {
    new TestTrees {
      assert(until(singleton, combine)(ctl1) === ctl1)
    }
  }

  test("until of ctl2 - two element list") {
    new TestTrees {
      assert(until(singleton, combine)(ctl2) === List(makeCodeTree(ct1b, ct2)))
    }
  }

  test("createCodeTree data test") {
    new TestBuildingTrees {
      assert(times(string2Chars(frenchString)) === frenchLetters)
    }
  }

  test("createCodeTree for French Letters") {
    new TestBuildingTrees {
      assert(createCodeTree(string2Chars(frenchString)) === frenchCode)
    }
  }

  test("decode example") {
    new TestBuildingTrees {
      val exampleTree: CodeTree = createCodeTree(string2Chars(exampleString))
      // println("exampleTree = " + exampleTree)
      assert(decode(exampleTree, exampleBits) === exampleBitsDecoded)
    }
  }

  test("decodedSecret") {
    new TestTrees {
      //    println("decodedSecret = " + decodedSecret)
      assert(decodedSecret === decodedSecretExample.toList)
    }
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      val encoded = encode(t1)("ab".toList)
      //      println("encoded = " + encoded)
      val decoded = decode(t1, encoded)
      //      println("decoded = " + decoded)
      assert(decoded === "ab".toList)
    }
  }

  test("convert code tree to code table for ctlExample") {
    new TestTrees {
      val codeTreeExample = until(singleton, combine)(ctlExample).head
      // println("codeTreeExample = " + codeTreeExample)
      val codeTableExample = convert(codeTreeExample)
      // println("codeTableExample = \n" + codeTableExample.mkString("\n"))
      assert(codeBits(codeTableExample)('b') === List[Bit](1, 1, 1))
    }
  }

  test("quickEncode of decodedSecretExample using frenchCode") {
    new TestTrees {
      val encodedSecret = quickEncode(frenchCode)(decodedSecretExample.toList)
      // println("encodedSecret = " + encodedSecret)
      assert(encodedSecret === secret)
    }
  }
}
