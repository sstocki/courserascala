package org.coursera.scala.lecture2

object lecture2 {
  println("Welcome to the Scala worksheet - higher order functions")
                                                  //> Welcome to the Scala worksheet - higher order functions

  // HIGHER ORDER FUNCTIONS

  //retunrs x - identity function
  def identity(x: Int): Int = x                   //> identity: (x: Int)Int

  // returns the sum of integers between a and b
  def sumInts(a: Int, b: Int): Int = {
    if (a > b) 0 else a + sumInts(a + 1, b)
  }                                               //> sumInts: (a: Int, b: Int)Int

  // returns x to power of 3 - x cube
  def cube(x: Int) = x * x * x                    //> cube: (x: Int)Int

  // returns the sum of the cubes of the integers between a and b
  def sumCubes(a: Int, b: Int): Int = {
    if (a > b) 0 else cube(a) + sumCubes(a + 1, b)
  }                                               //> sumCubes: (a: Int, b: Int)Int
 
  // returns factorial of x
  def fact(x: Int): Int = {
    if (x == 1) 1 else x * fact(x - 1)
  }                                               //> fact: (x: Int)Int

  // returns the sum of the factorials of the integers between a and b
  def sumFactorials(a: Int, b: Int): Int = {
    if (a > b) 0 else fact(a) + sumFactorials(a + 1, b)
  }                                               //> sumFactorials: (a: Int, b: Int)Int

  // Int => Int - function argument, gets Int and returns Int
  def sum(f: Int => Int, a: Int, b: Int): Int = {
    if (a > b) 0 else f(a) + sum(f, a + 1, b)
  }                                               //> sum: (f: Int => Int, a: Int, b: Int)Int

  // TAIL RECURSION
  def sumTail(f: Int => Int, a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else {
        loop(a + 1, f(a) + acc)
      }
    }
    loop(a, 0)
  }                                               //> sumTail: (f: Int => Int, a: Int, b: Int)Int

  // function returning function
  def sumFF(f: Int => Int): (Int, Int) => Int = {
    def sumF(a: Int, b: Int): Int = {
      if (a > b) 0
      else f(a) + sumF(a + 1, b)
    }
    sumF
  }                                               //> sumFF: (f: Int => Int)(Int, Int) => Int

  // function returning function - short notation
  def sumF(f: Int => Int)(a: Int, b: Int): Int =
    if (a > b) 0 else f(a) + sumF(f)(a + 1, b)    //> sumF: (f: Int => Int)(a: Int, b: Int)Int

  //returning product of integers between a and b using higher order functions
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 1 else f(a) * product(f)(a + 1, b)
  }                                               //> product: (f: Int => Int)(a: Int, b: Int)Int

  // combinre both sum and product functions into one function
  // they differ in:
  // - unit value
  // - combining operation
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, unitValue: Int)(a: Int, b: Int): Int = {
    if (a > b) unitValue else combine(f(a), mapReduce(f, combine, unitValue)(a + 1, b))
  }                                               //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, unitValue: Int)(a: I
                                                  //| nt, b: Int)Int

  sumInts(1, 10)                                  //> res0: Int = 55
  sum(identity, 1, 10)                            //> res1: Int = 55
  sum(x => x, 1, 10)                              //> res2: Int = 55
  sumTail(x => x, 1, 10)                          //> res3: Int = 55
  sumFF(identity)(1, 10)                          //> res4: Int = 55
  sumF(identity)(1, 10)                           //> res5: Int = 55
  mapReduce(identity, (x, y) => x + y, 0)(1, 10)  //> res6: Int = 55

  sumCubes(1, 10)                                 //> res7: Int = 3025
  sum(cube, 1, 10)                                //> res8: Int = 3025
  sum(a => a * a * a, 1, 10)                      //> res9: Int = 3025
  sumTail(a => a * a * a, 1, 10)                  //> res10: Int = 3025
  sumFF(cube)(1, 10)                              //> res11: Int = 3025
  sumF(cube)(1, 10)                               //> res12: Int = 3025
  mapReduce(cube, (x, y) => x + y, 0)(1, 10)      //> res13: Int = 3025

  sumFactorials(1, 10)                            //> res14: Int = 4037913
  sum(fact, 1, 10)                                //> res15: Int = 4037913
  sumTail(fact, 1, 10)                            //> res16: Int = 4037913
  sumFF(fact)(1, 10)                              //> res17: Int = 4037913
  sumF(fact)(1, 10)                               //> res18: Int = 4037913
  mapReduce(fact, (x, y) => x + y, 0)(1, 10)      //> res19: Int = 4037913

  // ANONYMOUS FUNCTIONS - we treat functions as a litrals
  (x: Int) => x * x * x                           //> res20: Int => Int = <function1>
  (x: Int, y: Int) => x + y                       //> res21: (Int, Int) => Int = <function2>
  (x: Int, y: Int) => x - y                       //> res22: (Int, Int) => Int = <function2>

  // EXERCISES

  product(x => x * x)(1, 5)                       //> res23: Int = 14400
  mapReduce(x => x * x, (x, y) => x * y, 1)(1, 5) //> res24: Int = 14400

  product(identity)(1, 5)                         //> res25: Int = 120
  mapReduce(identity, (x, y) => x * y, 1)(1, 5)   //> res26: Int = 120

  product(cube)(1, 5)                             //> res27: Int = 1728000
  mapReduce(cube, (x, y) => x * y, 1)(1, 5)       //> res28: Int = 1728000

  product(fact)(1, 5)                             //> res29: Int = 34560
  mapReduce(fact, (x, y) => x * y, 1)(1, 5)       //> res30: Int = 34560

  // write factorial in terms of product function
  def factP(x: Int): Int = {
    product(x => x)(1, x)
  }                                               //> factP: (x: Int)Int

  factP(3)                                        //> res31: Int = 6

  mapReduce(identity, (x, y) => x + y, 0)(1, 10)  //> res32: Int = 55

}