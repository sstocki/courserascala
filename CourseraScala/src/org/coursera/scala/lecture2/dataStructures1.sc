package org.coursera.scala.lecture2

object dataStructures {
  println("Welcome to the Scala worksheet - data structures")
                                                  //> Welcome to the Scala worksheet - data structures

  def r1 = new Rational(1, 2)                     //> r1: => org.coursera.scala.lecture2.Rational
  def r2 = new Rational(2, 3)                     //> r2: => org.coursera.scala.lecture2.Rational
  r1.numer                                        //> res0: Int = 1
  r1.denom                                        //> res1: Int = 2

  def addRational(r1: Rational, r2: Rational): Rational = {
    new Rational(
      r1.numer * r2.denom + r2.numer * r1.denom,
      r1.denom * r2.denom)
  }                                               //> addRational: (r1: org.coursera.scala.lecture2.Rational, r2: org.coursera.sca
                                                  //| la.lecture2.Rational)org.coursera.scala.lecture2.Rational

  def makeString(r: Rational) = r.numer + "/" + r.denom
                                                  //> makeString: (r: org.coursera.scala.lecture2.Rational)String

  makeString(addRational(new Rational(1, 2), new Rational(2, 3)))
                                                  //> res2: String = 7/6
  makeString(r1.add(r2))                          //> res3: String = 7/6
  r1.add(r2)                                      //> res4: org.coursera.scala.lecture2.Rational = 7/6

  val rX = new Rational(1, 3)                     //> rX  : org.coursera.scala.lecture2.Rational = 1/3
  val rY = new Rational(5, 7)                     //> rY  : org.coursera.scala.lecture2.Rational = 5/7
  val rZ = new Rational(3, 2)                     //> rZ  : org.coursera.scala.lecture2.Rational = 3/2
  val rA = new Rational(2,3)                      //> rA  : org.coursera.scala.lecture2.Rational = 2/3
  val rB = new Rational(6)                        //> rB  : org.coursera.scala.lecture2.Rational = 6/1

  val result = rX.sub(rY).sub(rZ)                 //> result  : org.coursera.scala.lecture2.Rational = -79/42
  rY.add(rY)                                      //> res5: org.coursera.scala.lecture2.Rational = 10/7
  rX add rA                                       //> res6: org.coursera.scala.lecture2.Rational = 1/1
  
  rX.less(rY)                                     //> res7: Boolean = true
  rX.max(rY)                                      //> res8: org.coursera.scala.lecture2.Rational = 5/7
   
  //val r0 = new Rational(1,0)
}
 
class Rational(x: Int, y: Int) {

  require(y != 0, "denominator must be nonzero")

  // Constructor
  def this(x : Int) = this(x, 1)

  //gcd - Greatest Common Divider
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  //private val g = gcd(x, y)
  //def numer = x / g
  //def denom = y / g
  val numer = x / gcd(x, y)
  val denom = y / gcd(x, y)

  def add(r: Rational) =
    new Rational(numer * r.denom + denom * r.numer, denom * r.denom)

  def neg: Rational = new Rational(-this.numer, this.denom)

  def sub(r: Rational) = this.add(r.neg)

  def less(r: Rational) = numer * r.denom < r.numer * denom

  def max(r: Rational) = if(this.less(r)) r else this
  
  override def toString = numer + "/" + denom

}