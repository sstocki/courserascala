package org.coursera.scala.lecture2

object dataStructures {
  println("Welcome to the Scala worksheet - data structures")
                                                  //> Welcome to the Scala worksheet - data structures

  def r1 = new Rational(1, 2)                     //> r1: => org.coursera.scala.lecture2.Rational
  def r2 = new Rational(2, 3)                     //> r2: => org.coursera.scala.lecture2.Rational
  r1.numer                                        //> res0: Int = 1
  r1.denom                                        //> res1: Int = 2

  def addRational(r1: Rational, r2: Rational): Rational = {
    new Rational(
      r1.numer * r2.denom + r2.numer * r1.denom,
      r1.denom * r2.denom)
  }                                               //> addRational: (r1: org.coursera.scala.lecture2.Rational, r2: org.coursera.sca
                                                  //| la.lecture2.Rational)org.coursera.scala.lecture2.Rational

  def makeString(r: Rational) = r.numer + "/" + r.denom
                                                  //> makeString: (r: org.coursera.scala.lecture2.Rational)String

  makeString(addRational(new Rational(1, 2), new Rational(2, 3)))
                                                  //> res2: String = 7/6
  makeString(r1 + r2)                             //> res3: String = 1/3
  r1 + r2                                         //> res4: org.coursera.scala.lecture2.Rational = 1/3

  val rX = new Rational(1, 2)                     //> rX  : org.coursera.scala.lecture2.Rational = 1/2
  val rY = new Rational(1, 3)                     //> rY  : org.coursera.scala.lecture2.Rational = 1/3
  val rZ = new Rational(3, 2)                     //> rZ  : org.coursera.scala.lecture2.Rational = 3/2
  val rA = new Rational(2, 3)                     //> rA  : org.coursera.scala.lecture2.Rational = 2/3
  val rB = new Rational(6)                        //> rB  : org.coursera.scala.lecture2.Rational = 6/1

  rX - rY - rZ                                    //> res5: org.coursera.scala.lecture2.Rational = 1/4
  -rX                                             //> res6: org.coursera.scala.lecture2.Rational = 1/-2
  rY + rY                                         //> res7: org.coursera.scala.lecture2.Rational = 1/9
  rX + rA                                         //> res8: org.coursera.scala.lecture2.Rational = 1/3

  rX < rY                                         //> res9: Boolean = false
  rX.max(rY)                                      //> res10: org.coursera.scala.lecture2.Rational = 1/2

  //val r0 = new Rational(1,0)
  rX * rX + rY * rY                               //> res11: org.coursera.scala.lecture2.Rational = 2/3
}

class Rational(x: Int, y: Int) {

  require(y != 0, "denominator must be nonzero")

  // Constructor
  def this(x: Int) = this(x, 1)

  //gcd - Greatest Common Divider
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  private val g = gcd(x, y)

  val numer = x / g
  val denom = y / g

  def +(r: Rational) =
    new Rational(numer * r.numer, denom * r.denom)

  def *(r: Rational) =
    new Rational(numer * r.denom + denom * r.numer, denom * r.denom)

  def unary_- : Rational = new Rational(-this.numer, this.denom)

  def -(r: Rational) = this + (-r)

  def <(r: Rational) = numer * r.denom < r.numer * denom

  def max(r: Rational) = if (this < r) r else this

  override def toString = numer + "/" + denom

}