package org.coursera.scala.lecture2

import math.abs

object findingFixedPoints {
  println("Welcome to the Scala worksheet - finding fixwd points")
                                                  //> Welcome to the Scala worksheet - finding fixwd points

  // fixed point of a function: x -> f(x) = x
  // eg. f(x) = 1 + x/2; fp = 2

  def isCloseEnough(x: Double, y: Double) =
    abs((x - y) / x) / x < 0.00001                //> isCloseEnough: (x: Double, y: Double)Boolean

  def fixedPoint(f: Double => Double)(firstGuess: Double) = {

    def iterate(guess: Double): Double = {
      val next = f(guess)
      if (isCloseEnough(guess, next)) next
      else iterate(next)
    }

    iterate(firstGuess)
  }                                               //> fixedPoint: (f: Double => Double)(firstGuess: Double)Double

  fixedPoint(x => 1 + x / 2)(1.0)                 //> res0: Double = 1.999969482421875

  // SQUARE ROOT sqrt(x) = y <=> y*y = x
  // y = x/y

  // DIVERGENT
  def sqrtDiv(x: Double) = fixedPoint(y => x / y)(1)
                                                  //> sqrtDiv: (x: Double)Double

  // CONVERGENT - averaging of successive values of the original sequence
  // instead of y = x/y we put: y = (y + x/y)/2
  def sqrtConv(x: Double) = fixedPoint(y => (y + x / y) / 2)(1)
                                                  //> sqrtConv: (x: Double)Double

  sqrtConv(2.0)                                   //> res1: Double = 1.4142135623746899
  sqrtConv(4.0)                                   //> res2: Double = 2.000000000000002

  def averageDamp(f: Double => Double)(x: Double) = (x + f(x)) / 2
                                                  //> averageDamp: (f: Double => Double)(x: Double)Double
  def sqrt(x: Double) = fixedPoint(averageDamp(y => x / y))(1)
                                                  //> sqrt: (x: Double)Double
  
  sqrtConv(2.0)                                   //> res3: Double = 1.4142135623746899
  sqrt(2.0)                                       //> res4: Double = 1.4142135623746899
  
  sqrtConv(4.0)                                   //> res5: Double = 2.000000000000002
  sqrt(4.0)                                       //> res6: Double = 2.000000000000002


}