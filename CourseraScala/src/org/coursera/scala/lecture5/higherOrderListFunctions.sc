package org.coursera.scala.lecture5

object higherOrderListFunctions {
  println("Welcome to the Scala worksheet- Higher-Order List Functions")
                                                  //> Welcome to the Scala worksheet- Higher-Order List Functions

  // simplified definition of methods: map, filter from class List
  //  abstract class List[T] {
  //
  //    def map[U](f: T => U): List[U] = this match {
  //      case Nil     => this
  //      case x :: xs => f(x) :: xs.map(f)
  //    }
  //    def filter[T](p: T => Boolean): List[T] = this match {
  //      case Nil => this
  //      case x :: xs => if(p(x)) x :: xs.filter(p) else xs.filter(p)
  //    }
  //  }

  // TRANSFORM each element of a list and return the list of results

  // multiply each element of a list by the same factor
  def scaleList1(xs: List[Double], factor: Double): List[Double] = xs match {
    case Nil     => xs
    case y :: ys => y * factor :: scaleList1(ys, factor)
  }                                               //> scaleList1: (xs: List[Double], factor: Double)List[Double]

  // multiply each element of a list by the same factor
  def scaleList(xs: List[Double], factor: Double): List[Double] = xs map (x => x * factor)
                                                  //> scaleList: (xs: List[Double], factor: Double)List[Double]

  def squareList1(xs: List[Int]): List[Int] = xs match {
    case Nil     => xs
    case y :: ys => y * y :: squareList1(ys)
  }                                               //> squareList1: (xs: List[Int])List[Int]

  def squareList(xs: List[Int]): List[Int] = xs.map(x => x * x)
                                                  //> squareList: (xs: List[Int])List[Int]

  val doubleList1 = List[Double](2.34, 5.64, 8.23, 4.09, 1.33)
                                                  //> doubleList1  : List[Double] = List(2.34, 5.64, 8.23, 4.09, 1.33)

  val factor = 8.0                                //> factor  : Double = 8.0
  scaleList1(doubleList1, factor)                 //> res0: List[Double] = List(18.72, 45.12, 65.84, 32.72, 10.64)
  scaleList(doubleList1, factor)                  //> res1: List[Double] = List(18.72, 45.12, 65.84, 32.72, 10.64)

  val intList1 = List(1, 2, 3, 4, 5, 6, 7, 8)     //> intList1  : List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)

  squareList1(intList1)                           //> res2: List[Int] = List(1, 4, 9, 16, 25, 36, 49, 64)
  squareList(intList1)                            //> res3: List[Int] = List(1, 4, 9, 16, 25, 36, 49, 64)

  // FILTER - select all elements satysfying a given condition

  // return all the positive elements of the list
  def posElems1(xs: List[Int]): List[Int] = xs match {
    case Nil     => xs
    case y :: ys => if (y > 0) y :: posElems1(ys) else posElems1(ys)
  }                                               //> posElems1: (xs: List[Int])List[Int]

  def posElems(xs: List[Int]): List[Int] = xs filter (x => x > 0)
                                                  //> posElems: (xs: List[Int])List[Int]

  val intList2 = List(1, 2, -3, 4, -5, 6, -7, -8, 9)
                                                  //> intList2  : List[Int] = List(1, 2, -3, 4, -5, 6, -7, -8, 9)
  
  posElems1(intList2)                             //> res4: List[Int] = List(1, 2, 4, 6, 9)
  posElems(intList2)                              //> res5: List[Int] = List(1, 2, 4, 6, 9)
}