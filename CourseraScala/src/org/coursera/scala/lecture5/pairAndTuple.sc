package org.coursera.scala.lecture5

object pairAndTuple {
  println("Welcome to the Scala worksheet - Pair and Tuple")
                                                  //> Welcome to the Scala worksheet - Pair and Tuple

  val intList = List(1)                           //> intList  : List[Int] = List(1)
  val intList1 = List(1, 2, 3, 4, 5, 6, 7, 8, 9)  //> intList1  : List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  val intList2 = List(11, 22, 33, 44, 55, 66, 77, 88, 99)
                                                  //> intList2  : List[Int] = List(11, 22, 33, 44, 55, 66, 77, 88, 99)

  val intList12 = intList1 ::: intList2           //> intList12  : List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 22, 33, 44, 55,
                                                  //|  66, 77, 88, 99)

  intList12 splitAt 3                             //> res0: (List[Int], List[Int]) = (List(1, 2, 3),List(4, 5, 6, 7, 8, 9, 11, 22,
                                                  //|  33, 44, 55, 66, 77, 88, 99))
  intList12 splitAt 9                             //> res1: (List[Int], List[Int]) = (List(1, 2, 3, 4, 5, 6, 7, 8, 9),List(11, 22,
                                                  //|  33, 44, 55, 66, 77, 88, 99))
  val pair1 = ("test", 1)                         //> pair1  : (String, Int) = (test,1)
  val (label, value) = pair1                      //> label  : String = test
                                                  //| value  : Int = 1
  
  
}