package org.coursera.scala.lecture5

object lecture5 {
  println("Welcome to the Scala worksheet - lecture 5")
                                                  //> Welcome to the Scala worksheet - lecture 5

  /* List - Operations */

  val intList = List(1)                           //> intList  : List[Int] = List(1)
  val intList1 = List(1, 2, 3, 4, 5, 6, 7, 8, 9)  //> intList1  : List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  val intList2 = List(11, 22, 33, 44, 55, 66, 77, 88, 99)
                                                  //> intList2  : List[Int] = List(11, 22, 33, 44, 55, 66, 77, 88, 99)

  intList1.length                                 //> res0: Int = 9
  // last element of a list
  intList1.last                                   //> res1: Int = 9

  // all elements of the list except the last one
  intList1.init                                   //> res2: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)
  intList.init                                    //> res3: List[Int] = List()

  intList1 take 4                                 //> res4: List[Int] = List(1, 2, 3, 4)
  intList1 take 12                                //> res5: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)

  intList1 drop 4                                 //> res6: List[Int] = List(5, 6, 7, 8, 9)
  intList1 drop 12                                //> res7: List[Int] = List()

  intList1(3)                                     //> res8: Int = 4
  intList1(8)                                     //> res9: Int = 9

  val intList12 = intList1 ++ intList2            //> intList12  : List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 22, 33, 44, 55,
                                                  //|  66, 77, 88, 99)
  intList12.reverse                               //> res10: List[Int] = List(99, 88, 77, 66, 55, 44, 33, 22, 11, 9, 8, 7, 6, 5, 4
                                                  //| , 3, 2, 1)

  intList2.updated(0, 100)                        //> res11: List[Int] = List(100, 22, 33, 44, 55, 66, 77, 88, 99)

  intList2 indexOf 11                             //> res12: Int = 0
  intList2 indexOf 100                            //> res13: Int = -1

  intList2 contains 55                            //> res14: Boolean = true
  intList2 contains 40                            //> res15: Boolean = false

  // concatenetion of lists
  intList1 ::: intList2                           //> res16: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 22, 33, 44, 55, 66, 7
                                                  //| 7, 88, 99)
  intList1 ++ intList2                            //> res17: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 22, 33, 44, 55, 66, 7
                                                  //| 7, 88, 99)

  /* Our own implementation of methods from class List */

  def lastMy[T](xs: List[T]): T = xs match {
    // any empty List
    case List()  => throw new Error("last of empty list")
    // any List with single element
    case List(x) => x
    // any List of length 2 or more
    case y :: ys => lastMy(ys)
  }                                               //> lastMy: [T](xs: List[T])T

  def initMy[T](xs: List[T]): List[T] = xs match {
    case List()  => throw new Error("init for empty list")
    case List(x) => List()
    case y :: ys => y :: initMy(ys)
  }                                               //> initMy: [T](xs: List[T])List[T]

  def concatMy[T](xs: List[T], ys: List[T]): List[T] = xs match {
    case List()  => ys
    case z :: zs => z :: concatMy(zs, ys)
  }                                               //> concatMy: [T](xs: List[T], ys: List[T])List[T]

  def reverseMy[T](xs: List[T]): List[T] = xs match {
    case List()  => xs
    case y :: ys => reverseMy(ys) ++ List(y)
  }                                               //> reverseMy: [T](xs: List[T])List[T]

  def removeAtMy[T](n: Int, xs: List[T]): List[T] = xs match {
    case List() => List()
    case y :: ys => {
      if (n == 0) ys
      else y :: removeAtMy(n - 1, ys)
    }
  }                                               //> removeAtMy: [T](n: Int, xs: List[T])List[T]

  def removeAtMyShort[T](n: Int, xs: List[T]): List[T] = (xs take n) ::: (xs drop n + 1)
                                                  //> removeAtMyShort: [T](n: Int, xs: List[T])List[T]

  def flatten(xs: List[Any]) : List[Any] = ???    //> flatten: (xs: List[Any])List[Any]
  
  val listToFlatten = List(List(1,1), 2, List(3, List(5,8)))
                                                  //> listToFlatten  : List[Any] = List(List(1, 1), 2, List(3, List(5, 8)))
  val listFlattened = List(1,1,2,3,5,8)           //> listFlattened  : List[Int] = List(1, 1, 2, 3, 5, 8)
  
  lastMy(intList1)                                //> res18: Int = 9
  lastMy(intList2)                                //> res19: Int = 99
  lastMy(intList)                                 //> res20: Int = 1

  initMy(intList1)                                //> res21: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)
  initMy(intList2)                                //> res22: List[Int] = List(11, 22, 33, 44, 55, 66, 77, 88)
  initMy(intList)                                 //> res23: List[Int] = List()

  concatMy(intList1, intList2)                    //> res24: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 22, 33, 44, 55, 66, 
                                                  //| 77, 88, 99)

  reverseMy(intList1)                             //> res25: List[Int] = List(9, 8, 7, 6, 5, 4, 3, 2, 1)
  reverseMy(intList2)                             //> res26: List[Int] = List(99, 88, 77, 66, 55, 44, 33, 22, 11)
  reverseMy(intList)                              //> res27: List[Int] = List(1)

  removeAtMy(1, intList1)                         //> res28: List[Int] = List(1, 3, 4, 5, 6, 7, 8, 9)
  removeAtMy(8, intList1)                         //> res29: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)
  removeAtMy(12, intList1)                        //> res30: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  removeAtMy(2, List())                           //> res31: List[Nothing] = List()
  
  removeAtMyShort(1, intList1)                    //> res32: List[Int] = List(1, 3, 4, 5, 6, 7, 8, 9)
  removeAtMyShort(8, intList1)                    //> res33: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)
  removeAtMyShort(12, intList1)                   //> res34: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  removeAtMyShort(2, List())                      //> res35: List[Nothing] = List()
}