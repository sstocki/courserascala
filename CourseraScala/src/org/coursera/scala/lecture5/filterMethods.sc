package org.coursera.scala.lecture5

object filterMethods {
  println("Welcome to the Scala worksheet - Filter Methods Variations")
                                                  //> Welcome to the Scala worksheet - Filter Methods Variations

  val intList = List(1, 2, 3, 4, -2, -6, -9, 7, -2, -3, 0)
                                                  //> intList  : List[Int] = List(1, 2, 3, 4, -2, -6, -9, 7, -2, -3, 0)
  val doubleList = List(2.0, -9.23, 134.7, 31.78, -0.456)
                                                  //> doubleList  : List[Double] = List(2.0, -9.23, 134.7, 31.78, -0.456)
  val stringList = List("pear", "apple", "lemon", "orange", "plum", "pineapple", "grape")
                                                  //> stringList  : List[String] = List(pear, apple, lemon, orange, plum, pineappl
                                                  //| e, grape)

  intList filter (x => x >= 0)                    //> res0: List[Int] = List(1, 2, 3, 4, 7, 0)
  intList filterNot (x => x >= 0)                 //> res1: List[Int] = List(-2, -6, -9, -2, -3)
  val (gte, lt) = intList partition (x => x >= 0) //> gte  : List[Int] = List(1, 2, 3, 4, 7, 0)
                                                  //| lt  : List[Int] = List(-2, -6, -9, -2, -3)
  gte                                             //> res2: List[Int] = List(1, 2, 3, 4, 7, 0)
  lt                                              //> res3: List[Int] = List(-2, -6, -9, -2, -3)

  // takes positive numbers while it does not reach negative one - longest prefix of the list
  intList takeWhile (x => x >= 0)                 //> res4: List[Int] = List(1, 2, 3, 4)

  // returns the remaining that was not taken by takeWhile
  intList dropWhile (x => x >= 0)                 //> res5: List[Int] = List(-2, -6, -9, 7, -2, -3, 0)

  intList span (x => x >= 0)                      //> res6: (List[Int], List[Int]) = (List(1, 2, 3, 4),List(-2, -6, -9, 7, -2, -3,
                                                  //|  0))

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 => {
      val (first, rest) = xs.span(y => y == x)
      first :: pack(rest)
    }
  }                                               //> pack: [T](xs: List[T])List[List[T]]

  def encode[T](xs: List[T]): List[(T,Int)] = {
    pack(xs).map (ys => (ys.head, ys.length))
  }                                               //> encode: [T](xs: List[T])List[(T, Int)]


  val toPack = List('a', 'a', 'a', 'b', 'c', 'c', 'a')
                                                  //> toPack  : List[Char] = List(a, a, a, b, c, c, a)
  
  pack(toPack)                                    //> res7: List[List[Char]] = List(List(a, a, a), List(b), List(c, c), List(a))
  encode(toPack)                                  //> res8: List[(Char, Int)] = List((a,3), (b,1), (c,2), (a,1))
  

}