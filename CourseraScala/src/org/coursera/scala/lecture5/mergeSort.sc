package org.coursera.scala.lecture5

object mergeSort {
  println("Welcome to the Scala worksheet - merge sort")
                                                  //> Welcome to the Scala worksheet - merge sort

  // MERGE SORT
  // If the list is empty or contains single element it is already sorted, otherwise:
  // 1. separate list into two sublist each containing around half of the elements of the original list
  // 2. sort the two sublists
  // 3. merge the two sorted sublists into a single sorted list
  def msortInt(xs: List[Int]): List[Int] = {

    def mergeOld(xs: List[Int], ys: List[Int]): List[Int] = xs match {
      case List() => ys
      case x :: xs1 => ys match {
        case List() => xs
        case y :: ys1 => {
          if (x < y) x :: merge(xs1, ys)
          else y :: merge(xs, ys1)
        }
      }
    }

    def merge(xs: List[Int], ys: List[Int]): List[Int] = (xs, ys) match {
      case (List(), ys) => ys
      case (xs, List()) => xs
      case (x :: xs1, y :: ys1) => {
        if (x < y) x :: merge(xs1, ys)
        else y :: merge(xs, ys1)
      }
    }

    val n = xs.length / 2
    if (n == 0) xs
    else {
      val (sublist1, sublist2) = xs splitAt n
      merge(msortInt(sublist1), msortInt(sublist2))
    }

  }                                               //> msortInt: (xs: List[Int])List[Int]

  def msort[T](xs: List[T])(lt: (T, T) => Boolean): List[T] = {

    def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
      case (List(), ys) => ys
      case (xs, List()) => xs
      case (x :: xs1, y :: ys1) => {
        if (lt(x, y)) x :: merge(xs1, ys)
        else y :: merge(xs, ys1)
      }
    }

    val n = xs.length / 2
    if (n == 0) xs
    else {
      val (sublist1, sublist2) = xs splitAt n
      merge(msort(sublist1)(lt), msort(sublist2)(lt))
    }
  }                                               //> msort: [T](xs: List[T])(lt: (T, T) => Boolean)List[T]

  // using Ordering class
  def msortOrd[T](xs: List[T])(ord: Ordering[T]): List[T] = {

    def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
      case (List(), ys) => ys
      case (xs, List()) => xs
      case (x :: xs1, y :: ys1) => {
        if (ord.lt(x, y)) x :: merge(xs1, ys)
        else y :: merge(xs, ys1)
      }
    }

    val n = xs.length / 2
    if (n == 0) xs
    else {
      val (sublist1, sublist2) = xs splitAt n
      merge(msortOrd(sublist1)(ord), msortOrd(sublist2)(ord))
    }
  }                                               //> msortOrd: [T](xs: List[T])(ord: Ordering[T])List[T]

  // using implicit Ordering, type of the Ordering is calculated base on the required type - T.
  def msortOrdImpl[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {

    def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
      case (List(), ys) => ys
      case (xs, List()) => xs
      case (x :: xs1, y :: ys1) => {
        if (ord.lt(x, y)) x :: merge(xs1, ys)
        else y :: merge(xs, ys1)
      }
    }

    val n = xs.length / 2
    if (n == 0) xs
    else {
      val (sublist1, sublist2) = xs splitAt n
      merge(msortOrdImpl(sublist1), msortOrdImpl(sublist2))
    }
  }                                               //> msortOrdImpl: [T](xs: List[T])(implicit ord: Ordering[T])List[T]

  val notSortedInt = List(4, 3, 5, 1, 8, 9, 2, 6, 7)
                                                  //> notSortedInt  : List[Int] = List(4, 3, 5, 1, 8, 9, 2, 6, 7)
  val notSortedString = List("apple", "pear", "orange", "banana", "melon", "lemon")
                                                  //> notSortedString  : List[String] = List(apple, pear, orange, banana, melon, 
                                                  //| lemon)

  msortInt(notSortedInt)                          //> res0: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  msort(notSortedInt)((x: Int, y: Int) => x < y)  //> res1: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  msortOrd(notSortedInt)(Ordering.Int)            //> res2: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  msortOrdImpl(notSortedInt)                      //> res3: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)

  // we do not neeed types in lt function
  msort(notSortedString)((x, y) => x.compareTo(y) < 0)
                                                  //> res4: List[String] = List(apple, banana, lemon, melon, orange, pear)
  msortOrd(notSortedString)(Ordering.String)      //> res5: List[String] = List(apple, banana, lemon, melon, orange, pear)
  msortOrdImpl(notSortedString)                   //> res6: List[String] = List(apple, banana, lemon, melon, orange, pear)

}