package org.coursera.scala.lecture5

object reductionOfLists {
  println("Welcome to the Scala worksheet - Reduction of Lists")
                                                  //> Welcome to the Scala worksheet - Reduction of Lists

  //  abstract class ListMy[T] {
  //
  //    def reduceLeft[T](op: (T, T) => T): T = this match {
  //      case Nil     => throw new Error("Nil.reduceLeft")
  //      case x :: xs => (xs foldLeft x)(op)
  //    }
  //
  //    def foldLeft[U](z: U)(op: (U, T) => U): U = this match {
  //      case Nil     => z
  //      case x :: xs => (xs foldLeft op(z, x))(op)
  //    }
  //  }

  def sum1(xs: List[Int]): Int = xs match {
    case Nil     => 0
    case y :: ys => y + sum1(ys)
  }                                               //> sum1: (xs: List[Int])Int

  def product1(xs: List[Int]): Int = xs match {
    case Nil     => 1
    case y :: ys => y * product1(ys)
  }                                               //> product1: (xs: List[Int])Int

  // reduceLeft inserts a given binary operator between adjecent elements of the list
  // reduceLeft can be applied only to NONEMPTY Lists
  def sum(xs: List[Int]): Int = (0 :: xs) reduceLeft ((x, y) => x + y)
                                                  //> sum: (xs: List[Int])Int
  // underscore _ represents the new parameter, going from left to right
  def sumS(xs: List[Int]): Int = (0 :: xs) reduceLeft (_ + _)
                                                  //> sumS: (xs: List[Int])Int
  // foldLeft is more general than reduceLeft, takes parameter accumolator, which is returned when xs is empty
  def sumF(xs: List[Int]): Int = (xs foldLeft 0)(_ + _)
                                                  //> sumF: (xs: List[Int])Int

  def product(xs: List[Int]): Int = (1 :: xs) reduceLeft ((x, y) => x * y)
                                                  //> product: (xs: List[Int])Int
  def productS(xs: List[Int]): Int = (1 :: xs) reduceLeft (_ * _)
                                                  //> productS: (xs: List[Int])Int
  def productF(xs: List[Int]): Int = (xs foldLeft 1)(_ * _)
                                                  //> productF: (xs: List[Int])Int

  def concat[T](xs: List[T], ys: List[T]): List[T] = (xs foldRight ys)(_ :: _)
                                                  //> concat: [T](xs: List[T], ys: List[T])List[T]

  // definition of function map based on foldRight
  def mapFun[T, U](xs: List[T], f: T => U): List[U] = {
    // (xs foldRight List[U]())((x, y) => f(x) :: y)
    (xs foldRight List[U]())(f(_) :: _)
  }                                               //> mapFun: [T, U](xs: List[T], f: T => U)List[U]

  // definition of function map based on foldRight
  def lenghtFun[T, U](xs: List[T]): Int = {
    (xs foldRight 0)((x, y) => y + 1)
  }                                               //> lenghtFun: [T, U](xs: List[T])Int

  val intList1 = List(1, 2, 3, 4, 5, 6, 7, 8, 9)  //> intList1  : List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  val intList2 = List(10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                                                  //> intList2  : List[Int] = List(10, 11, 12, 13, 14, 15, 16, 17, 18, 19)

  sum1(intList1)                                  //> res0: Int = 45
  sum(intList1)                                   //> res1: Int = 45
  sumS(intList1)                                  //> res2: Int = 45
  sumF(intList1)                                  //> res3: Int = 45

  product1(intList1)                              //> res4: Int = 362880
  product(intList1)                               //> res5: Int = 362880
  productS(intList1)                              //> res6: Int = 362880
  productF(intList1)                              //> res7: Int = 362880

  concat(intList1, intList1)                      //> res8: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9
                                                  //| )
  concat(intList1, intList2)                      //> res9: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 1
                                                  //| 6, 17, 18, 19)

  intList1.map(x => x * 3)                        //> res10: List[Int] = List(3, 6, 9, 12, 15, 18, 21, 24, 27)
  mapFun(intList1, (x: Int) => x * 3)             //> res11: List[Int] = List(3, 6, 9, 12, 15, 18, 21, 24, 27)
  intList1.map(_ * 3)                             //> res12: List[Int] = List(3, 6, 9, 12, 15, 18, 21, 24, 27)

  intList1.length                                 //> res13: Int = 9
  lenghtFun(intList1)                             //> res14: Int = 9
  intList2.length                                 //> res15: Int = 10
  lenghtFun(intList2)                             //> res16: Int = 10

}