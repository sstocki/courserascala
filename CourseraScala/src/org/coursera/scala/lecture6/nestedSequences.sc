package org.coursera.scala.lecture6

object nestedSequences {
  println("Welcome to the Scala worksheet - nested sequences")
                                                  //> Welcome to the Scala worksheet - nested sequences

  def isPrime(x: Int): Boolean = (2 until x).forall(x % _ != 0)
                                                  //> isPrime: (x: Int)Boolean

  def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double = (xs zip ys).map { case (x, y) => x * y }.sum
                                                  //> scalarProduct: (xs: Vector[Double], ys: Vector[Double])Double

  // Example 1
  // Given a positive integer n, find all pairs of positive integers i and j,
  // with 1 <= j < i < n such that i + j is prime number
  // e.g. for n = 7
  // i:     2; 3; 4; 4; 5; 6; 6;
  // j:     1; 2; 1; 3; 2; 1; 5;
  // i + j: 3; 5; 5; 7; 7; 7;11;

  val n = 7                                       //> n  : Int = 7

  (1 until n) map (i => (1 until i) map (j => (i, j)))
                                                  //> res0: scala.collection.immutable.IndexedSeq[scala.collection.immutable.Index
                                                  //| edSeq[(Int, Int)]] = Vector(Vector(), Vector((2,1)), Vector((3,1), (3,2)), V
                                                  //| ector((4,1), (4,2), (4,3)), Vector((5,1), (5,2), (5,3), (5,4)), Vector((6,1)
                                                  //| , (6,2), (6,3), (6,4), (6,5)))

  ((1 until n) map (i => (1 until i) map (j => (i, j)))).flatten
                                                  //> res1: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,1
                                                  //| ), (3,2), (4,1), (4,2), (4,3), (5,1), (5,2), (5,3), (5,4), (6,1), (6,2), (6,
                                                  //| 3), (6,4), (6,5))

  // map ... flatten <=> flatMap
  // flatMap reduces vector of vectors resulting from map to single vector

  (1 until n) flatMap (i => (1 until i) map (j => (i, j)))
                                                  //> res2: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,1
                                                  //| ), (3,2), (4,1), (4,2), (4,3), (5,1), (5,2), (5,3), (5,4), (6,1), (6,2), (6,
                                                  //| 3), (6,4), (6,5))

  // Final method solving the task from the example
  (1 until n) flatMap (i => (1 until i) map (j => (i, j))) filter (pair => isPrime(pair._1 + pair._2))
                                                  //> res3: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,
                                                  //| 2), (4,1), (4,3), (5,2), (6,1), (6,5))

  // Final method using for expression
  for {
    i <- 1 until n
    j <- 1 until i
    if isPrime(i + j)
  } yield (i, j)                                  //> res4: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,
                                                  //| 2), (4,1), (4,3), (5,2), (6,1), (6,5))

  def sumIsPrime(n: Int): Seq[(Int, Int)] = {
    //(1 until n) flatMap (i => (1 until i) map (j => (i, j))) filter (pair => isPrime(pair._1 + pair._2))
    for {
      i <- 1 until n
      j <- 1 until i
      if isPrime(i + j)
    } yield (i, j)
  }                                               //> sumIsPrime: (n: Int)Seq[(Int, Int)]

  sumIsPrime(8)                                   //> res5: Seq[(Int, Int)] = Vector((2,1), (3,2), (4,1), (4,3), (5,2), (6,1), (6
                                                  //| ,5), (7,4), (7,6))
  sumIsPrime(13)                                  //> res6: Seq[(Int, Int)] = Vector((2,1), (3,2), (4,1), (4,3), (5,2), (6,1), (6
                                                  //| ,5), (7,4), (7,6), (8,3), (8,5), (9,2), (9,4), (9,8), (10,1), (10,3), (10,7
                                                  //| ), (10,9), (11,2), (11,6), (11,8), (12,1), (12,5), (12,7), (12,11))
  sumIsPrime(21)                                  //> res7: Seq[(Int, Int)] = Vector((2,1), (3,2), (4,1), (4,3), (5,2), (6,1), (6
                                                  //| ,5), (7,4), (7,6), (8,3), (8,5), (9,2), (9,4), (9,8), (10,1), (10,3), (10,7
                                                  //| ), (10,9), (11,2), (11,6), (11,8), (12,1), (12,5), (12,7), (12,11), (13,4),
                                                  //|  (13,6), (13,10), (14,3), (14,5), (14,9), (15,2), (15,4), (15,8), (15,14), 
                                                  //| (16,1), (16,3), (16,7), (16,13), (16,15), (17,2), (17,6), (17,12), (17,14),
                                                  //|  (18,1), (18,5), (18,11), (18,13), (19,4), (19,10), (19,12), (19,18), (20,3
                                                  //| ), (20,9), (20,11), (20,17))

  // FOR EXPRESSION - equivalent to filter
  // the syntax: for( s ) yield e, where s i generator and/or filter
  // generator: p <- ppp; filter: if f
  // produces new result
 
  def ageOver1(personsList: List[Person], age: Int): List[String] = for (p <- personsList if p.age > age) yield p.name
                                                  //> ageOver1: (personsList: List[org.coursera.scala.lecture6.Person], age: Int)
                                                  //| List[String]
  def ageOver2(personsList: List[Person], age: Int): List[String] = personsList filter (p => p.age > age) map (p => p.name)
                                                  //> ageOver2: (personsList: List[org.coursera.scala.lecture6.Person], age: Int)
                                                  //| List[String]

  val persons = List(Person("Tom", 34), Person("John", 22), Person("Rob", 19), Person("Adam", 20), Person("Peter", 27),
    Person("Mike", 31), Person("Alan", 42))       //> persons  : List[org.coursera.scala.lecture6.Person] = List(Person(Tom,34), 
                                                  //| Person(John,22), Person(Rob,19), Person(Adam,20), Person(Peter,27), Person(
                                                  //| Mike,31), Person(Alan,42))

  persons.sortWith((p1, p2) => p1.age < p2.age)   //> res8: List[org.coursera.scala.lecture6.Person] = List(Person(Rob,19), Perso
                                                  //| n(Adam,20), Person(John,22), Person(Peter,27), Person(Mike,31), Person(Tom,
                                                  //| 34), Person(Alan,42))
  persons.sortWith(_.name < _.name)               //> res9: List[org.coursera.scala.lecture6.Person] = List(Person(Adam,20), Pers
                                                  //| on(Alan,42), Person(John,22), Person(Mike,31), Person(Peter,27), Person(Rob
                                                  //| ,19), Person(Tom,34))

  ageOver1(persons, 20)                           //> res10: List[String] = List(Tom, John, Peter, Mike, Alan)
  ageOver2(persons, 20)                           //> res11: List[String] = List(Tom, John, Peter, Mike, Alan)

  ageOver1(persons, 30)                           //> res12: List[String] = List(Tom, Mike, Alan)
  ageOver2(persons, 30)                           //> res13: List[String] = List(Tom, Mike, Alan)

  // Version of funtion calculating scalar product of two vestor using FOR EXPRESSION
  def scalarProductFor(xs: List[Double], ys: List[Double]): Double = {
    (for ((xi, yi) <- xs zip ys) yield xi * yi).sum
  }                                               //> scalarProductFor: (xs: List[Double], ys: List[Double])Double

  scalarProduct(Vector(3, 4, 5), Vector(1, 7, 9)) //> res14: Double = 76.0
  scalarProductFor(List(3, 4, 5), List(1, 7, 9))  //> res15: Double = 76.0
}

case class Person(name: String, age: Int)