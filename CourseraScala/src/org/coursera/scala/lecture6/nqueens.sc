package org.coursera.scala.lecture6

object nqueens {
  println("Welcome to the Scala worksheet - N-QUEEN PROBLEM")
                                                  //> Welcome to the Scala worksheet - N-QUEEN PROBLEM

  // Place eight queens on a chestboard (8x8) so no queen is threatened by another.
  // So it cannot be two queens in the same row, column or diagonal.
  // Develop general solution for solving this problem for any N.

  // ALGORITHM:
  // Place a queen in each row
  // If we placed (k-1) queens we must place the kth queen in a column that it is not "in check"
  // with any other queen placed on the board.
  // The one possible solution is the number of column in which we place queen for each row.
  // There can be one, many or none solution.

  def queens(n: Int): Set[List[Int]] = {

    def isSafe(col: Int, queens: List[Int]): Boolean = {
      val row: Int = queens.length
      val queensRC = (row - 1 to 0 by -1) zip queens
      queensRC forall {
        case (r, c) => col != c && math.abs(col - c) != row - r
      }
    }

    def placeQueens(k: Int): Set[List[Int]] = {
      if (k == 0) Set(List())
      else {
        for {
          queens <- placeQueens(k - 1)
          col <- 0 until n
          if isSafe(col, queens)
        } yield col :: queens
      }
    }
    placeQueens(n)
  }                                               //> queens: (n: Int)Set[List[Int]]
  
  def show(queens: List[Int]) = {
    val lines =
      for (col <- queens.reverse) yield
      Vector.fill(queens.length)(" 0 ").updated(col, " X ").mkString("")
    "\n" + lines.mkString("\n")
  }                                               //> show: (queens: List[Int])String
  
  (queens(4) map show).mkString("\n")             //> res0: String = "
                                                  //|  0  0  X  0 
                                                  //|  X  0  0  0 
                                                  //|  0  0  0  X 
                                                  //|  0  X  0  0 
                                                  //| 
                                                  //|  0  X  0  0 
                                                  //|  0  0  0  X 
                                                  //|  X  0  0  0 
                                                  //|  0  0  X  0 "
  (queens(8) map show).mkString("\n")             //> res1: String = "
                                                  //|  0  X  0  0  0  0  0  0 
                                                  //|  0  0  0  0  0  X  0  0 
                                                  //|  X  0  0  0  0  0  0  0 
                                                  //|  0  0  0  0  0  0  X  0 
                                                  //|  0  0  0  X  0  0  0  0 
                                                  //|  0  0  0  0  0  0  0  X 
                                                  //|  0  0  X  0  0  0  0  0 
                                                  //|  0  0  0  0  X  0  0  0 
                                                  //| 
                                                  //|  0  0  0  X  0  0  0  0 
                                                  //|  X  0  0  0  0  0  0  0 
                                                  //|  0  0  0  0  X  0  0  0 
                                                  //|  0  0  0  0  0  0  0  X 
                                                  //|  0  X  0  0  0  0  0  0 
                                                  //|  0  0  0  0  0  0  X  0 
                                                  //|  0  0  X  0  0  0  0  0 
                                                  //|  0  0  0  0  0  X  0  0 
                                                  //| 
                                                  //|  0  0  0  0  0  X  0  0 
                                                  //|  0  0  X  0  0  0  0  0 
                                                  //|  X  0  0  0  0  0  0  0 
                                                  //|  0  0  0  0  0  0  X  0 
                                                  //|  0  0  0  0  X  0  0  0 
                                                  //|  0  0  0  0  0  0  0  X 
                                                  //|  0  X  0  0  0  0  0  0 
                                                  //|  0  0  0  X  0  0  0  0 
                                                  //| 
                                                  //|  0  0  0  0  X  0  0  0 
                                                  //|  0  0  0  0  0  0  X  0 
                                                  //|  0  X  0  0  0  0  0  0 
                                                  //|  0  0  0  0  0  X  0  0 
                                                  //|  0  0  X  0  0  0  0  0 
                                                  //|  X  0  0  0  0  0  0  0 
                                                  //|  0  0  0  0  0  0  0  X 
                                                  //|  0  0  0  X  0  0  0  0 
  //queens(10)
}