package org.coursera.scala.lecture6

object maps {
  println("Welcome to the Scala worksheet - MAPS")//> Welcome to the Scala worksheet - MAPS

  // MAP is:
  // - a collection
  // - a function mapping Key => Value

  val romanNumerals = Map("I" -> 1, "V" -> 5, "X" -> 10, "L" -> 50, "C" -> 100, "D" -> 500, "M" -> 1000)
                                                  //> romanNumerals  : scala.collection.immutable.Map[String,Int] = Map(X -> 10, M
                                                  //|  -> 1000, I -> 1, V -> 5, L -> 50, C -> 100, D -> 500)
  val capitalOfCountry = Map("Poland" -> "Warsaw", "Czech Republic" -> "Prague", "Slovakia" -> "Bratislava")
                                                  //> capitalOfCountry  : scala.collection.immutable.Map[String,String] = Map(Pola
                                                  //| nd -> Warsaw, Czech Republic -> Prague, Slovakia -> Bratislava)

  romanNumerals("X")                              //> res0: Int = 10
  capitalOfCountry("Poland")                      //> res1: String = Warsaw
  capitalOfCountry get "Poland"                   //> res2: Option[String] = Some(Warsaw)
  capitalOfCountry get "Germany"                  //> res3: Option[String] = None

  // OPTION CLASS
  // trait Option[+A]
  // case class Some[+A](value: A) extends Option[A]
  // object None extends Option[Nothing]

  // we can use Option in pattern matching
  def showCapital(country: String, capitalOfCountry: Map[String, String]): String = {
    capitalOfCountry.get(country) match {
      case Some(capital) => capital
      case None          => "unknown capital"
    }
  }                                               //> showCapital: (country: String, capitalOfCountry: Map[String,String])String

  showCapital("Poland", capitalOfCountry)         //> res4: String = Warsaw
  showCapital("Germany", capitalOfCountry)        //> res5: String = unknown capital
}