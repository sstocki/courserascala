package org.coursera.scala.lecture6

object forExprAndHOF {
  println("Welcome to the Scala worksheet - FOR EXPRESSION and Higher Order Functions")
                                                  //> Welcome to the Scala worksheet - FOR EXPRESSION and Higher Order Functions

  // HOF like: map, flatMap and filter can be defined using FOR EXPRESSION

  def mapMy[T, U](xs: List[T], f: T => U): List[U] = for (x <- xs) yield f(x)
                                                  //> mapMy: [T, U](xs: List[T], f: T => U)List[U]

  def flatMapMy[T, U](xs: List[T], f: T => Iterable[U]): List[U] = for (x <- xs; y <- f(x)) yield y
                                                  //> flatMapMy: [T, U](xs: List[T], f: T => Iterable[U])List[U]

  def filterMy[T](xs: List[T], p: T => Boolean): List[T] = for (x <- xs if p(x)) yield x
                                                  //> filterMy: [T](xs: List[T], p: T => Boolean)List[T]

  def isPrime(x: Int): Boolean = (2 until x).forall(x % _ != 0)
                                                  //> isPrime: (x: Int)Boolean

  def sumIsPrime1(n: Int): Seq[(Int, Int)] = {
    (1 until n) flatMap (i => (1 until i) map (j => (i, j))) filter (pair => isPrime(pair._1 + pair._2))
  }                                               //> sumIsPrime1: (n: Int)Seq[(Int, Int)]

  def sumIsPrime2(n: Int): Seq[(Int, Int)] = {
    for {
      i <- 1 until n
      j <- 1 until i
      if isPrime(i + j)
    } yield (i, j)
  }                                               //> sumIsPrime2: (n: Int)Seq[(Int, Int)]

  // withFilter - lazy version of filter
  def sumIsPrime3(n: Int): Seq[(Int, Int)] = {
    (1 until n) flatMap (i => (1 until i).withFilter(j => isPrime(i + j)).map(j => (i, j)))
  }                                               //> sumIsPrime3: (n: Int)Seq[(Int, Int)]

  // Exercise - transform for expresion to higher-order functions
  def findBookTitleByAuthor1(books: List[Book], byAuthor: String): List[String] =
    for (book <- books; author <- book.authors if author.startsWith(byAuthor)) yield book.title
                                                  //> findBookTitleByAuthor1: (books: List[org.coursera.scala.lecture6.Book], byA
                                                  //| uthor: String)List[String]

  def findBookTitleByAuthor2(books: List[Book], byAuthor: String): List[String] = {
    books.flatMap(book => for (author <- book.authors if author.startsWith(byAuthor)) yield book.title)
  }                                               //> findBookTitleByAuthor2: (books: List[org.coursera.scala.lecture6.Book], byA
                                                  //| uthor: String)List[String]

  def findBookTitleByAuthor3(books: List[Book], byAuthor: String): List[String] = {
    books.flatMap(book => for (author <- book.authors withFilter (author => author.startsWith(byAuthor))) yield book.title)
  }                                               //> findBookTitleByAuthor3: (books: List[org.coursera.scala.lecture6.Book], byA
                                                  //| uthor: String)List[String]
  
  def findBookTitleByAuthor4(books: List[Book], byAuthor: String): List[String] = {
    books flatMap (b => b.authors withFilter (a => a startsWith byAuthor) map (y => b.title))
  }                                               //> findBookTitleByAuthor4: (books: List[org.coursera.scala.lecture6.Book], byA
                                                  //| uthor: String)List[String]

  BookDB.booksList                                //> res0: List[org.coursera.scala.lecture6.Book] = List(Book(Test ABC,List(Nowa
                                                  //| k, Adam, Kowalski, Jan)), Book(Java,List(Nowak, Adam, Pietrzak, Anna)), Boo
                                                  //| k(Java and Scala,List(Nowak, Marta, Ostrowki, Piotr)), Book(C#,List(Nowak, 
                                                  //| Adam, Kowalski, Antoni, Pawlak, Paweł)), Book(C# and C++,List(Pawlak, Pawe
                                                  //| ł)), Book(Java and C#,List(Sienkiewicz, Iwona, Gruszka, Ewa)), Book(Scala 
                                                  //| and Groovy,List(Nowak, Anna, Kowalski Tomasz)), Book(Groovy, Scala and Java
                                                  //| ,List(Ostrowki, Piotr, Nowacki, Adam)), Book(C# and ANSI C,List(Kowalczyk, 
                                                  //| Jan)))
  
  findBookTitleByAuthor1(BookDB.booksList, "Nowak")
                                                  //> res1: List[String] = List(Test ABC, Java, Java and Scala, C#, Scala and Gro
                                                  //| ovy)
  findBookTitleByAuthor2(BookDB.booksList, "Nowak")
                                                  //> res2: List[String] = List(Test ABC, Java, Java and Scala, C#, Scala and Gro
                                                  //| ovy)
  findBookTitleByAuthor3(BookDB.booksList, "Nowak")
                                                  //> res3: List[String] = List(Test ABC, Java, Java and Scala, C#, Scala and Gro
                                                  //| ovy)
  findBookTitleByAuthor4(BookDB.booksList, "Nowak")
                                                  //> res4: List[String] = List(Test ABC, Java, Java and Scala, C#, Scala and Gro
                                                  //| ovy)

}