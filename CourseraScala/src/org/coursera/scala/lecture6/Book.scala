package org.coursera.scala.lecture6

/**
 * @author sylwesterstocki
 */
case class Book(title: String, authors: List[String])

object BookDB {

  val booksList = List(
    Book("Test ABC", List("Nowak, Adam", "Kowalski, Jan")),
    Book("Java", List("Nowak, Adam", "Pietrzak, Anna")),
    Book("Java and Scala", List("Nowak, Marta", "Ostrowki, Piotr")),
    Book("C#", List("Nowak, Adam", "Kowalski, Antoni", "Pawlak, Paweł")),
    Book("C# and C++", List("Pawlak, Paweł")),
    Book("Java and C#", List("Sienkiewicz, Iwona", "Gruszka, Ewa")),
    Book("Scala and Groovy", List("Nowak, Anna", "Kowalski Tomasz")),
    Book("Groovy, Scala and Java", List("Ostrowki, Piotr", "Nowacki, Adam")),
    Book("C# and ANSI C", List("Kowalczyk, Jan")))

  val booksSet = Set(
    Book("Test ABC", List("Nowak, Adam", "Kowalski, Jan")),
    Book("Java", List("Nowak, Adam", "Pietrzak, Anna")),
    Book("Java and Scala", List("Nowak, Marta", "Ostrowki, Piotr")),
    Book("C#", List("Nowak, Adam", "Kowalski, Antoni", "Pawlak, Paweł")),
    Book("C# and C++", List("Pawlak, Paweł")),
    Book("Java and C#", List("Sienkiewicz, Iwona", "Gruszka, Ewa")),
    Book("Scala and Groovy", List("Nowak, Anna", "Kowalski Tomasz")),
    Book("Groovy, Scala and Java", List("Ostrowki, Piotr", "Nowacki, Adam")),
    Book("C# and ANSI C", List("Kowalczyk, Jan")))

}