package org.coursera.scala.lecture6

object translatePhoneNumber {
  println("Welcome to the Scala worksheet - Translate Phone Number")
                                                  //> Welcome to the Scala worksheet - Translate Phone Number

  // TASK
  // Phone keys have mnemonics assigned to them.
  // Assume you are given a dictionary words as a list of words.
  // Design a method translate such that translate(phoneNumber)
  // produces all phrases of words that can serve as mnemonics for the phone number.
  // Example: The phone number “7225247386” should have the mnemonic:
  // "Scala is fun" as one element of the set of solution phrases.

  val dictionaryURL = "http://lamp.epfl.ch/files/content/sites/lamp/files/teaching/progfun/linuxwords.txt"
                                                  //> dictionaryURL  : String = http://lamp.epfl.ch/files/content/sites/lamp/files
                                                  //| /teaching/progfun/linuxwords.txt
  // Read file from a given URL
  val inputDistionary = scala.io.Source.fromURL(dictionaryURL)
                                                  //> inputDistionary  : scala.io.BufferedSource = non-empty iterator

  // Read dictionary, each word is in separate line
  // Filter out words that contains non-letter characters
  val distionary = inputDistionary.getLines.toList filter (word => word forall (char => char.isLetter))
                                                  //> distionary  : List[String] = List(Aarhus, Aaron, Ababa, aback, abaft, abando
                                                  //| n, abandoned, abandoning, abandonment, abandons, abase, abased, abasement, a
                                                  //| basements, abases, abash, abashed, abashes, abashing, abasing, abate, abated
                                                  //| , abatement, abatements, abater, abates, abating, Abba, abbe, abbey, abbeys,
                                                  //|  abbot, abbots, Abbott, abbreviate, abbreviated, abbreviates, abbreviating, 
                                                  //| abbreviation, abbreviations, Abby, abdomen, abdomens, abdominal, abduct, abd
                                                  //| ucted, abduction, abductions, abductor, abductors, abducts, Abe, abed, Abel,
                                                  //|  Abelian, Abelson, Aberdeen, Abernathy, aberrant, aberration, aberrations, a
                                                  //| bet, abets, abetted, abetter, abetting, abeyance, abhor, abhorred, abhorrent
                                                  //| , abhorrer, abhorring, abhors, abide, abided, abides, abiding, Abidjan, Abig
                                                  //| ail, Abilene, abilities, ability, abject, abjection, abjections, abjectly, a
                                                  //| bjectness, abjure, abjured, abjures, abjuring, ablate, ablated, ablates, abl
                                                  //| ating, ablation, ablativ
                                                  //| Output exceeds cutoff limit.

  val mnemonics = Map('2' -> "ABC", '3' -> "DEF", '4' -> "GHI", '5' -> "JKL", '6' -> "MNO", '7' -> "PQRS",
    '8' -> "TUV", '9' -> "WXYZ")                  //> mnemonics  : scala.collection.immutable.Map[Char,String] = Map(8 -> TUV, 4 
                                                  //| -> GHI, 9 -> WXYZ, 5 -> JKL, 6 -> MNO, 2 -> ABC, 7 -> PQRS, 3 -> DEF)

  val charCode: Map[Char, Char] = for ((digit, letters) <- mnemonics; letter <- letters) yield letter -> digit
                                                  //> charCode  : Map[Char,Char] = Map(E -> 3, X -> 9, N -> 6, T -> 8, Y -> 9, J 
                                                  //| -> 5, U -> 8, F -> 3, A -> 2, M -> 6, I -> 4, G -> 4, V -> 8, Q -> 7, L -> 
                                                  //| 5, B -> 2, P -> 7, C -> 2, H -> 4, W -> 9, K -> 5, R -> 7, O -> 6, D -> 3, 
                                                  //| Z -> 9, S -> 7)

  // Returns digit code for a single word using charCode map
  def wordCode(word: String): String = {
    word.toUpperCase map charCode
  }                                               //> wordCode: (word: String)String

  // Map from digit string to the words that represents them,
  // a missing number should map to the empty set.
  // We introduce default value, when there is no word for a number sequence
  val wordsForNum: Map[String, Seq[String]] = distionary groupBy wordCode withDefaultValue Seq()
                                                  //> wordsForNum  : Map[String,Seq[String]] = Map(63972278 -> List(newscast), 29
                                                  //| 237638427 -> List(cybernetics), 782754448 -> List(starlight), 2559464 -> Li
                                                  //| st(allying), 862532733 -> List(uncleared), 365692259 -> List(enjoyably), 86
                                                  //| 8437 -> List(unties), 33767833 -> List(deportee), 742533 -> List(picked), 3
                                                  //| 364646489 -> List(femininity), 3987267346279 -> List(extraordinary), 785539
                                                  //| 7 -> List(pulleys), 67846493 -> List(optimize), 4723837 -> List(grafter), 3
                                                  //| 86583 -> List(evolve), 78475464 -> List(Stirling), 746459 -> List(singly), 
                                                  //| 847827 -> List(vistas), 546637737 -> List(lionesses), 28754283 -> List(curl
                                                  //| icue), 84863372658 -> List(thunderbolt), 46767833 -> List(imported), 264374
                                                  //| 64 -> List(angering, cohering), 8872267 -> List(turbans), 77665377 -> List(
                                                  //| spoolers), 46636233 -> List(homemade), 7446768759 -> List(rigorously), 7464
                                                  //| 4647 -> List(ringings), 633738 -> List(offset), 847825 -> List(visual), 772
                                                  //| 832 -> List(Pravda), 47
                                                  //| Output exceeds cutoff limit.

  // Returns all waysto encode a umber as a list of words from a dictionary.
  // We split our number into substrings
  // We find words for first of a split and encode the rest of a split
  def encode(number: String): Set[List[String]] = {
    if (number.isEmpty()) Set(List())
    else {
      for {
        splitIndex <- 1 to number.length
        words <- wordsForNum(number.take(splitIndex))
        rest <- encode(number.drop(splitIndex))
      } yield words :: rest
    }.toSet
  }                                               //> encode: (number: String)Set[List[String]]

  def translate(number: String): Set[String] = encode(number) map (_ mkString " ")
                                                  //> translate: (number: String)Set[String]

  wordCode("Java")                                //> res0: String = 5282
  wordCode("JAVA")                                //> res1: String = 5282

  encode("7225247386")                            //> res2: Set[List[String]] = Set(List(rack, ah, re, to), List(sack, ah, re, to
                                                  //| ), List(Scala, ire, to), List(sack, air, fun), List(rack, air, fun), List(r
                                                  //| ack, bird, to), List(pack, air, fun), List(pack, ah, re, to), List(pack, bi
                                                  //| rd, to), List(Scala, is, fun), List(sack, bird, to))
  translate("7225247386")                         //> res3: Set[String] = Set(sack air fun, pack ah re to, pack bird to, Scala ir
                                                  //| e to, Scala is fun, rack ah re to, pack air fun, sack bird to, rack bird to
                                                  //| , sack ah re to, rack air fun)

}