package org.coursera.scala.lecture6

object sets {
  println("Welcome to the Scala worksheet - SETS")//> Welcome to the Scala worksheet - SETS

  // SET
  // - unordered
  // - contain unique elements
  // - fundamental method: CONTAINS

  val fruitSet = Set("apple", "banana", "lemon", "pear", "grapefruit", "plum", "cranberry")
                                                  //> fruitSet  : scala.collection.immutable.Set[String] = Set(plum, banana, cranb
                                                  //| erry, apple, pear, lemon, grapefruit)
  val rangeSet = (1 to 21 by 3).toSet             //> rangeSet  : scala.collection.immutable.Set[Int] = Set(10, 1, 13, 7, 16, 19, 
                                                  //| 4)
  val set1to10 = (1 to 10).toSet                  //> set1to10  : scala.collection.immutable.Set[Int] = Set(5, 10, 1, 6, 9, 2, 7, 
                                                  //| 3, 8, 4)
  set1to10 map (_ + 2)                            //> res0: scala.collection.immutable.Set[Int] = Set(5, 10, 6, 9, 12, 7, 3, 11, 8
                                                  //| , 4)
  set1to10 map (_ / 2)                            //> res1: scala.collection.immutable.Set[Int] = Set(0, 5, 1, 2, 3, 4)
  fruitSet filter (_.length() > 4)                //> res2: scala.collection.immutable.Set[String] = Set(banana, cranberry, apple,
                                                  //|  lemon, grapefruit)
  rangeSet.nonEmpty                               //> res3: Boolean = true
  rangeSet contains 5                             //> res4: Boolean = false
  set1to10 contains 5                             //> res5: Boolean = true

}