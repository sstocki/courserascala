package org.coursera.scala.lecture6

object queeriesGroupAndOrder {
  println("Welcome to the Scala worksheet - GROUP BY and ORDER BY")
                                                  //> Welcome to the Scala worksheet - GROUP BY and ORDER BY

  val fruits = List("apple", "pear", "lemon", "plum", "orange", "banana")
                                                  //> fruits  : List[String] = List(apple, pear, lemon, plum, orange, banana)

  // ORDER BY is done using SORTWITH and SORTED

  fruits sortWith (_.length < _.length)           //> res0: List[String] = List(pear, plum, apple, lemon, orange, banana)
  fruits.sorted                                   //> res1: List[String] = List(apple, banana, lemon, orange, pear, plum)

  // GROUP BY is available in Scala
  // groups fruits by first letter
  fruits groupBy (l => l.head)                    //> res2: scala.collection.immutable.Map[Char,List[String]] = Map(a -> List(appl
                                                  //| e), b -> List(banana), l -> List(lemon), p -> List(pear, plum), o -> List(or
                                                  //| ange))
  
  
}