package org.coursera.scala.lecture6

object polynomials {
  println("Welcome to the Scala worksheet - POLYNOMIAL - map implementation")
                                                  //> Welcome to the Scala worksheet - POLYNOMIAL - map implementation

  val pA1 = new Polynomial(Map(1 -> 2.0, 3 -> 4.0, 5 -> 6.2))
                                                  //> pA1  : org.coursera.scala.lecture6.Polynomial = 6.2x^5 + 4.0x^3 + 2.0x^1
  val pA2 = new Polynomial(Map(0 -> 3.0, 3 -> 7.0))
                                                  //> pA2  : org.coursera.scala.lecture6.Polynomial = 7.0x^3 + 3.0x^0
  pA1 + pA2                                       //> res0: org.coursera.scala.lecture6.Polynomial = 6.2x^5 + 11.0x^3 + 2.0x^1 + 3
                                                  //| .0x^0

  val pB1 = new PolynomialExt(1 -> 2.0, 3 -> 4.0, 5 -> 6.2)
                                                  //> pB1  : org.coursera.scala.lecture6.PolynomialExt = 6.2x^5 + 4.0x^3 + 2.0x^1
  val pB2 = new PolynomialExt(Map(0 -> 3.0, 3 -> 7.0))
                                                  //> pB2  : org.coursera.scala.lecture6.PolynomialExt = 7.0x^3 + 3.0x^0
  pB1 + pB2                                       //> res1: org.coursera.scala.lecture6.PolynomialExt = 6.2x^5 + 11.0x^3 + 2.0x^1 
                                                  //| + 3.0x^0
 
}

class Polynomial(val terms: Map[Int, Double]) {

  def +(other: Polynomial): Polynomial = new Polynomial(terms ++ (other.terms map adjust))

  // we check if in this Polynomial there is a coefficient with the same exponent and if so we add them
  def adjust(term: (Int, Double)): (Int, Double) = {
    val (exp, coeff) = term
    terms get exp match {
      case Some(c) => exp -> (coeff + c)
      case None    => exp -> coeff
    }
  }

  override def toString = {
    (for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + "x^" + exp).mkString(" + ")
  }
}

class PolynomialExt(termsParam: Map[Int, Double]) {

  // * represents repeated parameters - sequence of parameters
  def this(bindings: (Int, Double)*) = this(bindings.toMap)

  // we introduce default value to avoid exception while retriving unknown element from Map terms
  // that leads to simplification of adjust method
  val terms = termsParam withDefaultValue 0.0

  def +(other: PolynomialExt): PolynomialExt = new PolynomialExt((other.terms foldLeft terms)(addTerm))

  // we check if in this Polynomial there is a coefficient with the same exponent and if so we add them
  def addTerm(terms: Map[Int, Double], term: (Int, Double)): Map[Int, Double] = {
    val (exp, coeff) = term
    terms + (exp -> (coeff + terms(exp)))
  }

  override def toString = {
    (for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + "x^" + exp).mkString(" + ")
  }
}