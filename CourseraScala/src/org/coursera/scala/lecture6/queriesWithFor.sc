package org.coursera.scala.lecture6

object queriesWithFor {
  println("Welcome to the Scala worksheet - QUERIES WITH FOR")
                                                  //> Welcome to the Scala worksheet - QUERIES WITH FOR

  BookDB.booksList                                //> res0: List[org.coursera.scala.lecture6.Book] = List(Book(Test ABC,List(Nowak
                                                  //| , Adam, Kowalski, Jan)), Book(Java,List(Nowak, Adam, Pietrzak, Anna)), Book(
                                                  //| Java and Scala,List(Nowak, Marta, Ostrowki, Piotr)), Book(C#,List(Nowak, Ada
                                                  //| m, Kowalski, Antoni, Pawlak, Paweł)), Book(C# and C++,List(Pawlak, Paweł))
                                                  //| , Book(Java and C#,List(Sienkiewicz, Iwona, Gruszka, Ewa)), Book(Scala and G
                                                  //| roovy,List(Nowak, Anna, Kowalski Tomasz)), Book(Groovy, Scala and Java,List(
                                                  //| Ostrowki, Piotr, Nowacki, Adam)), Book(C# and ANSI C,List(Kowalczyk, Jan)))
  BookDB.booksSet                                 //> res1: scala.collection.immutable.Set[org.coursera.scala.lecture6.Book] = Set
                                                  //| (Book(C# and ANSI C,List(Kowalczyk, Jan)), Book(Java and C#,List(Sienkiewicz
                                                  //| , Iwona, Gruszka, Ewa)), Book(Groovy, Scala and Java,List(Ostrowki, Piotr, N
                                                  //| owacki, Adam)), Book(Java and Scala,List(Nowak, Marta, Ostrowki, Piotr)), Bo
                                                  //| ok(Test ABC,List(Nowak, Adam, Kowalski, Jan)), Book(Java,List(Nowak, Adam, P
                                                  //| ietrzak, Anna)), Book(C# and C++,List(Pawlak, Paweł)), Book(C#,List(Nowak, 
                                                  //| Adam, Kowalski, Antoni, Pawlak, Paweł)), Book(Scala and Groovy,List(Nowak, 
                                                  //| Anna, Kowalski Tomasz)))

  def findBookTitleByAuthor(books: List[Book], byAuthor: String): List[String] =
    for (book <- books; author <- book.authors if author.startsWith(byAuthor)) yield book.title
                                                  //> findBookTitleByAuthor: (books: List[org.coursera.scala.lecture6.Book], byAut
                                                  //| hor: String)List[String]

  findBookTitleByAuthor(BookDB.booksList, "Nowak")//> res2: List[String] = List(Test ABC, Java, Java and Scala, C#, Scala and Groo
                                                  //| vy)
  findBookTitleByAuthor(BookDB.booksList, "Nowak, Adam")
                                                  //> res3: List[String] = List(Test ABC, Java, C#)

  // find all names of authors who wrote at least 2 books
  def findAuthorOf2Books(books: Set[Book]) = {
    for {
      b1 <- books
      b2 <- books
      if b1.title < b2.title
      a1 <- b1.authors
      a2 <- b2.authors
      if a1 == a2
    } yield a1
  }                                               //> findAuthorOf2Books: (books: Set[org.coursera.scala.lecture6.Book])scala.coll
                                                  //| ection.immutable.Set[String]

  def findAuthorOf2BooksDistinct(books: List[Book]) = {
    {for {
      b1 <- books
      b2 <- books
      if b1.title < b2.title
      a1 <- b1.authors
      a2 <- b2.authors
      if a1 == a2
    } yield a1} distinct
  }                                               //> findAuthorOf2BooksDistinct: (books: List[org.coursera.scala.lecture6.Book])L
                                                  //| ist[String]

  findAuthorOf2Books(BookDB.booksSet)             //> res4: scala.collection.immutable.Set[String] = Set(Ostrowki, Piotr, Nowak, A
                                                  //| dam, Pawlak, Paweł)
  
  findAuthorOf2BooksDistinct(BookDB.booksList)    //> res5: List[String] = List(Nowak, Adam, Pawlak, Paweł, Ostrowki, Piotr)

}