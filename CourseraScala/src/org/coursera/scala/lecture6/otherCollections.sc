package org.coursera.scala.lecture6

object otherCollections {
  println("Welcome to the Scala worksheet - Other Collections")
                                                  //> Welcome to the Scala worksheet - Other Collections

  // Vector - sequence implementation
  // it is implemented as an array of arrays of pointers with depth N
  // access complexity for single element is log32(N), where N - depth of vector

  // VECTOR - OPERATIONS

  val intVector1 = Vector(1, 2, 3, 4, 5, 6, 7, 8, 9)
                                                  //> intVector1  : scala.collection.immutable.Vector[Int] = Vector(1, 2, 3, 4, 5,
                                                  //|  6, 7, 8, 9)
  val intList1 = intVector1.toList                //> intList1  : List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)

  val intList2 = List(10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                                                  //> intList2  : List[Int] = List(10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
  val intVector2 = intList2.toVector              //> intVector2  : Vector[Int] = Vector(10, 11, 12, 13, 14, 15, 16, 17, 18, 19)

  val intV1 = 0 +: intVector1                     //> intV1  : scala.collection.immutable.Vector[Int] = Vector(0, 1, 2, 3, 4, 5, 6
                                                  //| , 7, 8, 9)
  val intV2 = intVector2 :+ 20                    //> intV2  : scala.collection.immutable.Vector[Int] = Vector(10, 11, 12, 13, 14,
                                                  //|  15, 16, 17, 18, 19, 20)

  // Iterable:
  // Sequence:         Set:           Map:
  // List:  Vector:

  // Operations for lists are also applicable to: Vector, Array and String
  val intArray = Array(1, 2, 3, 4, 5, 6, 7, 8, 9) //> intArray  : Array[Int] = Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
  val intList = List(23, 5, 34, 21, 6, -9, -45, 25, 61)
                                                  //> intList  : List[Int] = List(23, 5, 34, 21, 6, -9, -45, 25, 61)
  val intVector = Vector(9, 7, 3, 1, 4, 8, 6, 0, 2)
                                                  //> intVector  : scala.collection.immutable.Vector[Int] = Vector(9, 7, 3, 1, 4, 
                                                  //| 8, 6, 0, 2)
  val charVector = Vector('a', 'b', 'c', 'd', 'e', 'f')
                                                  //> charVector  : scala.collection.immutable.Vector[Char] = Vector(a, b, c, d, e
                                                  //| , f)
  val charArray = Array('a', 'b', 'c', 'd', 'e', 'f')
                                                  //> charArray  : Array[Char] = Array(a, b, c, d, e, f)
  val string1 = "Hello World"                     //> string1  : String = Hello World

  // Array[Int](1, 2, 3).map((x: Int) => x + 4)
  // intArray.map(x => x * 2)
  string1.filter(c => c.isUpper)                  //> res0: String = HW

  val range1un5: Range = 1 until 5                //> range1un5  : Range = Range(1, 2, 3, 4)
  val range1to5: Range = 1 to 5                   //> range1to5  : Range = Range(1, 2, 3, 4, 5)
  val range0to15by3: Range = 0 to 15 by 3         //> range0to15by3  : Range = Range(0, 3, 6, 9, 12, 15)
  val range100to0by10: Range = 100 to 0 by -10    //> range100to0by10  : Range = Range(100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0
                                                  //| )

  // OTHER operations on sequences

  // EXISTS
  intArray exists (_ == 8)                        //> res1: Boolean = true
  intArray exists (x => x == 100)                 //> res2: Boolean = false

  // FORALL
  intArray forall (_ > 0)                         //> res3: Boolean = true
  intArray forall (_ > 5)                         //> res4: Boolean = false

  // ZIP
  intList zip charArray                           //> res5: List[(Int, Char)] = List((23,a), (5,b), (34,c), (21,d), (6,e), (-9,f)
                                                  //| )
  intVector zip string1                           //> res6: scala.collection.immutable.Vector[(Int, Char)] = Vector((9,H), (7,e),
                                                  //|  (3,l), (1,l), (4,o), (8, ), (6,W), (0,o), (2,r))

  // UNZIP
  (intList zip charVector).unzip                  //> res7: (List[Int], List[Char]) = (List(23, 5, 34, 21, 6, -9),List(a, b, c, d
                                                  //| , e, f))

  // FLATMAP
  intVector.flatMap(i => List(-i, i))             //> res8: scala.collection.immutable.Vector[Int] = Vector(-9, 9, -7, 7, -3, 3, 
                                                  //| -1, 1, -4, 4, -8, 8, -6, 6, 0, 0, -2, 2)
  intList.flatMap(i => List(i, 2 * i))            //> res9: List[Int] = List(23, 46, 5, 10, 34, 68, 21, 42, 6, 12, -9, -18, -45, 
                                                  //| -90, 25, 50, 61, 122)
  string1 flatMap (c => if (c == 'o') List('$', c) else List(c))
                                                  //> res10: String = Hell$o W$orld

  intList.sum                                     //> res11: Int = 121
  intList.product                                 //> res12: Int = -663545516
  intList.min                                     //> res13: Int = -45
  intList.max                                     //> res14: Int = 61

  // APPLICATIONS AND EXAMPLES

  // list of all combinations of numbers (1 to M) and (1 to N)
  def combi(m: Int, n: Int): Seq[(Int, Int)] =
    (1 to m).flatMap(x => (1 to n).map(y => (x, y)))
                                                  //> combi: (m: Int, n: Int)Seq[(Int, Int)]

  def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double = {
    // val zipped = xs zip ys
    // val multi = zipped.map(xy => xy._1 * xy._2)
    // multi.sum
    // (xs zip ys).map(xy => xy._1 * xy._2).sum
    (xs zip ys).map { case (x, y) => x * y }.sum
  }                                               //> scalarProduct: (xs: Vector[Double], ys: Vector[Double])Double

  def isPrime(x: Int): Boolean = {
    // val r = 1 to x
    // val filter = r.filter(i => x % i == 0)
    // filter.length == 2
    // (1 to x).filter(x % _ == 0).length == 2
    // (2 to x - 1) forall (i => x % i != 0)
    (2 until x).forall(x % _ != 0)
  }                                               //> isPrime: (x: Int)Boolean

  combi(10, 5)                                    //> res15: Seq[(Int, Int)] = Vector((1,1), (1,2), (1,3), (1,4), (1,5), (2,1), (
                                                  //| 2,2), (2,3), (2,4), (2,5), (3,1), (3,2), (3,3), (3,4), (3,5), (4,1), (4,2),
                                                  //|  (4,3), (4,4), (4,5), (5,1), (5,2), (5,3), (5,4), (5,5), (6,1), (6,2), (6,3
                                                  //| ), (6,4), (6,5), (7,1), (7,2), (7,3), (7,4), (7,5), (8,1), (8,2), (8,3), (8
                                                  //| ,4), (8,5), (9,1), (9,2), (9,3), (9,4), (9,5), (10,1), (10,2), (10,3), (10,
                                                  //| 4), (10,5))

  scalarProduct(Vector(1, 2, 3), Vector(2, 4, 7)) //> res16: Double = 31.0

  isPrime(21)                                     //> res17: Boolean = false
  isPrime(5)                                      //> res18: Boolean = true
  isPrime(19)                                     //> res19: Boolean = true
}