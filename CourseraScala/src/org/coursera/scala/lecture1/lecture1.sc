package org.coursera.scala.lecture1

object lecture1 {
  println("Welcome to the Scala worksheet - lecture1")
  //> Welcome to the Scala worksheet - lecture1
  // non-terminant statement
  def loop: Boolean = loop //> loop: => Boolean

  // operator => means: call by name
  def and(x: Boolean, y: => Boolean): Boolean = if (x) y else false
  //> and: (x: Boolean, y: => Boolean)Boolean

  and(true, true) //> res0: Boolean = true
  and(false, true) //> res1: Boolean = false
  and(false, loop) //> res2: Boolean = false

  // operator => means: call by name
  def or(x: Boolean, y: => Boolean): Boolean = if (x) true else y
  //> or: (x: Boolean, y: => Boolean)Boolean
  or(true, false) //> res3: Boolean = true
  or(false, false) //> res4: Boolean = false
  or(true, loop) //> res5: Boolean = true

  def abs(x: Double) = if (x >= 0) x else -x //> abs: (x: Double)Double

  def mean(x: Double, y: Double): Double = (x + y) / 2
  //> mean: (x: Double, y: Double)Double

  // block
  def sqrt(x: Double): Double = {

    // definition is visible only inside the block
    def sqrtIter(guess: Double = 1, x: Double): Double = {
      if (isGoodEnough(guess, x)) guess
      else sqrtIter(improve(guess, x), x)
    }

    def isGoodEnough(guess: Double, x: Double, epsilon: Double = 0.0001): Boolean =
      abs(guess * guess - x) / x < epsilon

    def improve(guess: Double, x: Double): Double = mean(guess, x / guess)

    sqrtIter(x = x)
  } //> sqrt: (x: Double)Double

  sqrt(2) //> res6: Double = 1.4142156862745097
  sqrt(4) //> res7: Double = 2.0000000929222947
  sqrt(1e-6) //> res8: Double = 0.0010000001533016628
  sqrt(1e60) //> res9: Double = 1.0000000031080746E30

  // SCOPE RULES
  val x = 0 //> x  : Int = 0
  def f(y: Int) = y + 1 //> f: (y: Int)Int
  val result = {
    // x variable is shadowed but f() function is visible
    val x = f(3)
    x * x
  } + x //> result  : Int = 16

  // cleaner version, get read of redundant x parameter
  def sqrtC(x: Double): Double = {

    // definition is visible only inside the block
    def sqrtIter(guess: Double): Double = {
      if (isGoodEnough(guess, 0.0001)) guess
      else sqrtIter(improve(guess))
    }

    def isGoodEnough(guess: Double, epsilon: Double): Boolean =
      abs(guess * guess - x) / x < epsilon

    def improve(guess: Double): Double = mean(guess, x / guess)

    sqrtIter(1.0)
  } //> sqrtC: (x: Double)Double

  sqrtC(2) //> res10: Double = 1.4142156862745097
  sqrtC(4) //> res11: Double = 2.0000000929222947
  sqrtC(1e-6) //> res12: Double = 0.0010000001533016628
  sqrtC(1e60) //> res13: Double = 1.0000000031080746E30

  // TAIL RECURSION

  // greatest common divider: Euclyde algorithm
  def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b) //> gcd: (a: Int, b: Int)Int

  gcd(14, 21) //> res14: Int = 7
  gcd(21, 14) //> res15: Int = 7

  def factorial(n: Int): Int =
    if (n == 0) 1 else n * factorial(n - 1) //> factorial: (n: Int)Int

  factorial(4) //> res16: Int = 24
  factorial(12) //> res17: Int = 479001600

  def factorialTail(n: Int): Int = {

    def loop(acc: Int, n: Int): Int = {
      if (n == 0) acc
      else loop(acc * n, n - 1)
    }
    loop(1, n)
  }
}