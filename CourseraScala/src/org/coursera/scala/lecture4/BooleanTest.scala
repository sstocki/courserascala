package org.coursera.scala.lecture4

/**
 * @author sylwesterstocki
 */
abstract class BooleanTest {

  def ifThenElse[T](t: T, e: T): T

  def && (x: => BooleanTest): BooleanTest = ifThenElse(x, falseA)
  def || (x: => BooleanTest): BooleanTest = ifThenElse(trueA, x)
  def unary_! : BooleanTest = ifThenElse(falseA, trueA)
  
  def == (x:BooleanTest): BooleanTest = ifThenElse(x, x.unary_!)
  def != (x:BooleanTest): BooleanTest = ifThenElse(x.unary_!, x)
  
  def < (x: BooleanTest): BooleanTest = ifThenElse(falseA, x)
  def > (x: BooleanTest): BooleanTest = ifThenElse(x, falseA)
}

object trueA extends BooleanTest {
  def ifThenElse[T](t: T, e: T): T = t
}

object falseA extends BooleanTest {
  def ifThenElse[T](t: T, e: T): T = e
}