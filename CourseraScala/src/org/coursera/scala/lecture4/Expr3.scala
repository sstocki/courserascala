package org.coursera.scala.lecture4

/**
 * @author sylwesterstocki
 */

/**
 * Uses pattern matching for creating expressions on numbers.
 * Use pattern matching if you often add new functionality (methods) to the existing type hierarchy.
 */
trait Expr3 {
  case class Number3(n: Int) extends Expr3
  case class Sum3(e1: Expr3, e2: Expr3) extends Expr3

  def eval: Int = this match {
    case Number3(n)   => n
    case Sum3(e1, e2) => e1.eval + e2.eval
  }

  def show: String = this match {
    case Number3(n)   => "Number = " + n
    case Sum3(e1, e2) => e1.show + " + " + e2.show
  }
}
