package org.coursera.scala.lecture4


/**
 * @author sylwesterstocki
 */

/**
 * We want the Nil to be an object (singleton).
 * We use covariance (the + sign) to achieve that.
 */
trait ListCovariant[+T] {

  /**
   * Returns true if list is empty.
   */
  def isEmpty: scala.Boolean

  /**
   * Returns first element of a list
   */
  def head: T

  /**
   * Returns remaining part of a list (without head).
   */
  def tail: ListCovariant[T]
  
  /**
   * Returns the new list 
   */
  // covariance incorrect
  // def prepend(elem: T): ListCovariant[T] = new ConsCovariant(elem, this)
  // U must be a supertype of list element type T
  def prepend[U >: T](elem: U): ListCovariant[U] = new ConsCovariant(elem, this)
}

/**
 * Implementation of a list as a Cons-List - Immutable Linked List.
 */
// Polymorphism: subtyping - instance of a subclass can be passed to a base class
// Polymorphism: generics - instance of a function or class is created by type parametrisation

// head and tail are defined in the definition
class ConsCovariant[T](val head: T, val tail: ListCovariant[T]) extends ListCovariant[T] {

  def isEmpty: scala.Boolean = false
}

/**
 * We use class Nothing because it is subtype o every class
 */
object NilCovariant extends ListCovariant[Nothing] {

  def isEmpty: scala.Boolean = true
  def head: Nothing = throw new NoSuchElementException("Nil.head")
  def tail: Nothing = throw new NoSuchElementException("Nil.tail")
}

object test {
  
  // in that case List[Nothing] is subtype of List[String]
  val x : ListCovariant[String] = NilCovariant
  
//  def f(xs: ListCovariant[NonEmpty], x : Empty) = xs prepend x
  
  
}