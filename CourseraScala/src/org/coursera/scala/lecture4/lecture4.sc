package org.coursera.scala.lecture4


object lecture4 {
  println("Welcome to the Scala worksheet - lecture4")
                                                  //> Welcome to the Scala worksheet - lecture4
 
  /* LISTS */
  // Lists are immutable - the elements of a list cannot be changed (arrays are mutable)
  // Lists are recurcive (arrays are flat)
  // Lists are homogeneous - all elements of list must have the same type

  /**
   * Returns new list that has all elements of list xs sorted in descending order.
   * It is insertion sort algorithm, comlexity O(N*N)
   */
  def isort(xs: List[Int]): List[Int] = xs match {
    case List() => List()
    // :: - pattern matching; y - head of a list, ys the tail of a list
    case y :: ys      => insert(y, isort(ys))
  }                                               //> isort: (xs: List[Int])List[Int]

  def insert(x: Int, xs: List[Int]): List[Int] = xs match {
    case List() => List(x)
    case y :: ys => if(x <= y) x :: xs else y :: insert(x, ys)
  }                                               //> insert: (x: Int, xs: List[Int])List[Int]

  // :: is construction operation for the lists - cons
  // x :: xs <=> constructs new list with the first element x followed by elements of xs

  val fruits1 : List[String] = List[String]("apples", "oranges", "pears")
                                                  //> fruits1  : List[String] = List(apples, oranges, pears)
  val fruits2 = "raspberries" :: ("cranberries" :: ("strawberries" :: Nil))
                                                  //> fruits2  : List[String] = List(raspberries, cranberries, strawberries)
  
  val nums1 = List(1, 2, 3, 4)                    //> nums1  : List[Int] = List(1, 2, 3, 4)
  val nums2 = 1 :: 2 :: 3 :: 4 :: Nil             //> nums2  : List[Int] = List(1, 2, 3, 4)
  
  // list of lists
  val diag3 = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1))
                                                  //> diag3  : List[List[Int]] = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1)
                                                  //| )
  val empty1 = List()                             //> empty1  : List[Nothing] = List()
  val empty2 = Nil                                //> empty2  : scala.collection.immutable.Nil.type = List()

  fruits1.head                                    //> res0: String = apples
  fruits1.tail                                    //> res1: List[String] = List(oranges, pears)
  fruits2.isEmpty                                 //> res2: Boolean = false
 
  val toSort = List(7, 3, 9, 2)                   //> toSort  : List[Int] = List(7, 3, 9, 2)
  val sorted = isort(toSort)                      //> sorted  : List[Int] = List(2, 3, 7, 9)

}