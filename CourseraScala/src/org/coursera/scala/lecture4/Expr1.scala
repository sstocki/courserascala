package org.coursera.scala.lecture4

/**
 * @author sylwesterstocki
 */

/**
 * Represents the expression on numbers.
 */
trait Expr1 {

  def isNumber: scala.Boolean
  def isSum: scala.Boolean
  def numValue: Int
  def leftOp: Expr1
  def rightOp: Expr1

  def eval(e: Expr1): Int = {
    if (e.isNumber) e.numValue
    else if (e.isSum) eval(e.leftOp) + eval(e.rightOp)
    else throw new Error("Unknown expression " + e)
  }

}

class Number1(n: Int) extends Expr1 {

  def isNumber: scala.Boolean = true
  def isSum: scala.Boolean = false
  def numValue: Int = n
  def leftOp: Expr1 = throw new Error("Number.leftOp")
  def rightOp: Expr1 = throw new Error("Number.rightOp")
}

/**
 *
 * new Sum(e1,e2) <=> e1 + e2
 */
class Sum1(e1: Expr1, e2: Expr1) extends Expr1 {

  def isNumber: scala.Boolean = false
  def isSum: scala.Boolean = true
  def numValue: Int = throw new Error("Sum.numValue")
  def leftOp: Expr1 = e1
  def rightOp: Expr1 = e2
}