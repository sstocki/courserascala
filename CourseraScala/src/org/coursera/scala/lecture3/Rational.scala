package org.coursera.scala.lecture3

/**
 * @author sylwesterstocki
 */
// Scala is a single inheritance language => class can inherit from only one superclass
class Rational(x: Int, y: Int) {

  require(y != 0, "denominator must be nonzero")

  // Constructor
  def this(x: Int) = this(x, 1)

  //gcd - Greatest Common Divider
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  private val g = gcd(x, y)

  val numer = x / g
  val denom = y / g

  def +(r: Rational) =
    new Rational(numer * r.numer, denom * r.denom)

  def *(r: Rational) =
    new Rational(numer * r.denom + denom * r.numer, denom * r.denom)

  def unary_- : Rational = new Rational(-this.numer, this.denom)

  def -(r: Rational) = this + (-r)

  def <(r: Rational) = numer * r.denom < r.numer * denom

  def max(r: Rational) = if (this < r) r else this

  override def toString = numer + "/" + denom

}