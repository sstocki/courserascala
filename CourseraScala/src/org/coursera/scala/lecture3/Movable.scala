package org.coursera.scala.lecture3

/**
 * @author sylwesterstocki
 */
trait Movable {
  
  def move(x: Int, y: Int) : (Int, Int)
}