package org.coursera.scala.lecture3

/**
 * @author sylwesterstocki
 */
class Square(a: Int) extends Shape with Planar with Movable {

  def height: Int = a
  def width: Int = a

  def move(x: Int, y: Int): (Int, Int) = {
    (a + x, a + y)
  }

}