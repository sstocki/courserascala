package org.coursera.scala.lecture3.conslist

object lecture3 {
  println("Welcome to the Scala worksheet - Const-List")
                                                  //> Welcome to the Scala worksheet - Const-List

  def singleton[T](elem: T) = new Cons(elem, new Nil[T])
                                                  //> singleton: [T](elem: T)org.coursera.scala.lecture3.conslist.Cons[T]

  /**
   * Returns the n'th element of the list.
   */
  def nth[T](n: Int, list: ListCons[T]): T = {
    if (list.isEmpty) throw new IndexOutOfBoundsException()
    else if (n == 0) list.head
    else nth(n - 1, list.tail)
  }                                               //> nth: [T](n: Int, list: org.coursera.scala.lecture3.conslist.ListCons[T])T

  singleton[Int](1)                               //> res0: org.coursera.scala.lecture3.conslist.Cons[Int] = org.coursera.scala.le
                                                  //| cture3.conslist.Cons@498654ae
  singleton[Boolean](false)                       //> res1: org.coursera.scala.lecture3.conslist.Cons[Boolean] = org.coursera.scal
                                                  //| a.lecture3.conslist.Cons@12219138
  singleton("singleton")                          //> res2: org.coursera.scala.lecture3.conslist.Cons[String] = org.coursera.scala
                                                  //| .lecture3.conslist.Cons@59c4fcd4

  val list123 = new Cons(1, new Cons(2, new Cons(3, new Nil)))
                                                  //> list123  : org.coursera.scala.lecture3.conslist.Cons[Int] = org.coursera.sca
                                                  //| la.lecture3.conslist.Cons@13b2f057
  //nth[Int](4, list123)
  //nth[Int](-1, list123)
  nth[Int](2, list123)                            //> res3: Int = 3
  nth[Int](0, list123)                            //> res4: Int = 1

  ListT.l0                                        //> res5: List[Nothing] = List()
  ListT.l1                                        //> res6: List[String] = List(test1)
  ListT.l2                                        //> res7: List[Int] = List(12, 32)
  
}

object ListT{

  def apply[T](): ListCons[T] = new Nil
  def apply[T](t: T): ListCons[T] = new Cons(t, new Nil)
  def apply[T](t1: T, t2: T): ListCons[T] = new Cons[T](t1, new Cons[T](t2, new Nil[T]))

  def l0 = List()
  def l1 = List("test1")
  def l2 = List(12, 32)

}

trait ListCons[T] {

  /**
   * Returns true if list is empty.
   */
  def isEmpty: Boolean

  /**
   * Returns first element of a list
   */
  def head: T

  /**
   * Returns remaining part of a list (without head).
   */
  def tail: ListCons[T]
}

/**
 * Implementation of a list as a Cons-List - Immutable Linked List.
 */
// Polymorphism: subtyping - instance of a subclass can be passed to a base class
// Polymorphism: generics - instance of a function or class is created by type parametrisation

// head and tail are defined in the definition
class Cons[T](val head: T, val tail: ListCons[T]) extends ListCons[T] {

  def isEmpty: Boolean = false
}

class Nil[T] extends ListCons[T] {

  def isEmpty: Boolean = true
  def head: Nothing = throw new NoSuchElementException("Nil.head")
  def tail: Nothing = throw new NoSuchElementException("Nil.tail")
}