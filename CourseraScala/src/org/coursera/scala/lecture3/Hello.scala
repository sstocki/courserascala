package org.coursera.scala.lecture3

/**
 * @author sylwesterstocki
 */
object Hello {

  def main(args: Array[String]) = println("Hello world")

}