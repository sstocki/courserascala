package org.coursera.scala.lecture3

object base {
  println("Welcome to the Scala worksheet Base classes")
                                                  //> Welcome to the Scala worksheet Base classes
}

abstract class Base {
  def foo = 1
  def bar: Int
}

class Sub extends Base {
  // overrides the definition of foo from superclass
  override def foo = 2
  def bar = 3
}