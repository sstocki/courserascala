package org.coursera.scala.lecture3

/**
 * @author sylwesterstocki
 */
// trait is like abstract class that can provide multiple inheritance (like in C++)
// similar but more powerful than interface in Java
// it can contain fields and concrete method implementations
trait Planar {
  
  def height : Int
  def width : Int
  
  // default implementation of method surface, can be overwriten in subclasses
  def surface = height * width
}