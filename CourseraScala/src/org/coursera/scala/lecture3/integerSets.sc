package org.coursera.scala.lecture3

object integerSets {
  println("Welcome to the Scala worksheet - Integer Sets")

  val set1 = new NonEmpty(3, new Empty, new Empty)
  val set2 = set1 incl 4
  val set3 = set1 incl 1 incl 12

}

abstract class IntSet {

  /**
   * Includes an element x into IntSet.
   */
  def incl(x: Int): IntSet

  /**
   * Returns true, if given set contains integer x.
   */
  def contains(x: Int): Boolean

  /**
   * Returns a union of a given set and set `s`.
   */
  def union(s: IntSet): IntSet
}

/**
 * Implementation of a Set in the form of Binary Tree.
 */

class Empty extends IntSet {

  def incl(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)
  def contains(x: Int): Boolean = false
  def union(s: IntSet): IntSet = s

  override def toString = "."
}

class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {

  def incl(x: Int): IntSet = {
    if (x < elem) new NonEmpty(x, left incl x, right)
    else if (x > elem) new NonEmpty(x, left, right incl x)
    else this
  }

  def contains(x: Int): Boolean = {
    if (x < elem) left contains x
    else if (x > elem) right contains x
    else true
  }

  def union(s: IntSet): IntSet = {
    // this terminates because we always deal with smaller than initial set  - left, right.
    ((left union right) union s) incl elem
  }

  override def toString = "{" + left + elem + right + "}"
}

// it is Singleton thanks to word: object
object EmptySingleton extends IntSet {

  def incl(x: Int): IntSet = new NonEmpty(x, EmptySingleton, EmptySingleton)
  def contains(x: Int): Boolean = false

  override def toString = "."
}