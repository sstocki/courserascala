package org.coursera.scala.lecture3

object scratch {

  // different tipes of imports
  import org.coursera.scala.lecture2._
  import org.coursera.scala.lecture3.Rational
  import org.coursera.scala.lecture3.{Rational, Hello}
  
  println("Welcome to the Scala worksheet - scratch")
                                                  //> Welcome to the Scala worksheet - scratch
  
  val r1 = new Rational(2,3)                      //> r1  : org.coursera.scala.lecture3.Rational = 2/3
  
  // report an error using throw
  def error(message: String) = throw new Error(message)
                                                  //> error: (message: String)Nothing
  //error("test message")
  
  // Null is a class for representing empty reference objects
  val aNull = null                                //> aNull  : Null = null
  val sNull : String = aNull                      //> sNull  : String = null
  
  // Cannot initialize primitive type with null - compilation error
  //val x : Int = null
  
  // what is the type of that expression: it is the first common ancestor of Int and Boolean
  if(true) 1 else false                           //> res0: AnyVal = 1
}