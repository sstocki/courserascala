package funsets

object Main extends App {
  import FunSets._

  println(singletonSet(5))
  println(singletonSet(7))

  println(contains(singletonSet(1), 1))
  println(contains(singletonSet(1), 5))

  printSet(union(singletonSet(4), singletonSet(6)))

  val setNegative: Set = (x: Int) => x < 0
  val setNegativeWith0: Set = (x: Int) => x <= 0
  val set0To100: Set = (x: Int) => 0 <= x && x <= 100
  val setNeg100To100: Set = (x: Int) => -100 <= x && x <= 100
  val setPositiveWith0: Set = (x: Int) => x > 0
  val setAbove50: Set = (x: Int) => x > 50
  val setDiv5: Set = (x: Int) => x % 5 == 0
  val set1: Set = singletonSet(1)
  val set2: Set = singletonSet(2)
  val set12: Set = union(set1, set2)
  val set0 = diff(setNegative, setNegative)

  val fAll = (x: Int) => true

  println(contains(setNegative, 89))
  println(contains(setNegative, -7))

  println(contains(set0To100, 123))
  println(contains(set0To100, 14))

  printSet(intersect(setNegative, setAbove50))
  printSet(filter(setNegative, setAbove50))

  println("EXISTS TEST"); println()

  println("exists(setNegative, setAbove50)")
  println(exists(setNegative, setAbove50))
//  println(exists2(setNegative, setAbove50))
//  println(existsIter(setNegative, setAbove50))

  println("exists(set0To100, setNegativeWith0)")
  println(exists(set0To100, setNegativeWith0))
//  println(exists2(set0To100, setNegativeWith0))
//  println(existsIter(set0To100, setNegativeWith0))

  println("exists(set1, set12)")
  println(exists(set1, set12))
//  println(exists2(set1, set12))
//  println(existsIter(set1, set12))

  def add3(x: Int): Int = x + 3
  val setDiv5Add3 = (x: Int) => (x + 3) % 5 == 0
  printSet(map(setDiv5, add3))
  printSet(map(set0, add3))

  println("s0.isEmpty = " + isEmpty(set0))

  println(); println("forALL2 test - set0")
//  println(forall2(set0, fAll))
  println(forall(set0, fAll))

  println(); println("forALL2 test - set1")
//  println(forall2(set1, set1))
  println(forall(set1, set1))

  println(); println("forALL2 test - set12")
//  println(forall2(set2, set12))
  println(forall(set2, set12))

  println("forALL2 test end")
}
