package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {

  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s0 = diff(s1, s1)
    val set10To100: Set = (x: Int) => 10 <= x && x <= 100
    val setNegative: Set = (x: Int) => x < 0
    val setDiv5: Set = (x: Int) => x % 5 == 0

    val fPositive = (x: Int) => x > 0

    val fDivisibleBy2 = (x: Int) => x % 2 == 0
    val fGreaterThan10 = (x: Int) => x > 10
    val fGreaterThan1 = (x: Int) => x > 1
    val fGreaterThanNeg2 = (x: Int) => x > -2
    val fAll = (x: Int) => true
    val fAdd3 = (x: Int) => x + 3
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2) // = {1,2}
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }
  
  test("union with empty contains all elements") {
    new TestSets {
      val s = union(s0, s1) // = {1}
      assert(contains(s, 1), "Union 1")
      assert(!contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersection contains common elements") {
    new TestSets {
      val s1s2 = union(s1, s2) // = {1,2}
      val s2s3 = union(s2, s3) // = {2,3}

      val s = intersect(s1s2, s2s3) // = {2}
      assert(!contains(s, 1), "Intersection 1")
      assert(contains(s, 2), "Intersection 2")
      assert(!contains(s, 3), "Intersection 3")
    }
  }
  
    test("intersection with contains zero elements") {
    new TestSets {
      val s = intersect(s0, s1) // = {}
      assert(!contains(s, 1), "Intersection 1")
      assert(!contains(s, 2), "Intersection 2")
      assert(!contains(s, 3), "Intersection 3")
    }
  }

  test("difference contains elements from first set not from the second") {
    new TestSets {
      val s1s2 = union(s1, s2) // = {1,2}
      val s2s3 = union(s2, s3) // = {2,3}

      val s = diff(s1s2, s2s3) // = {1}
      assert(contains(s, 1), "Difference 1")
      assert(!contains(s, 2), "Difference 2")
      assert(!contains(s, 3), "Difference 3")
    }
  }
  
  test("difference from empty contains zero elements") {
    new TestSets {
      val s = diff(s0, s1) // = {}
      assert(!contains(s, 1), "Difference 1")
      assert(!contains(s, 2), "Difference 2")
      assert(!contains(s, 3), "Difference 3")
    }
  }
  
  test("difference subtract empty contains all elements from first set") {
    new TestSets {
      val s = diff(s1, s0) // = {1}
      assert(contains(s, 1), "Difference 1")
      assert(!contains(s, 2), "Difference 2")
      assert(!contains(s, 3), "Difference 3")
    }
  }

  test("filtered contains elements from set that fulfill the filter condition") {
    new TestSets {
      val s1s2 = union(s1, s2) // = {1,2}

      val s = filter(s1s2, fGreaterThan1) // s = {2}
      assert(!contains(s, 1), "Filter 1")
      assert(contains(s, 2), "Filter 2")
      assert(!contains(s, 3), "Filter 3")
    }
  }

  test("filtered for empty set") {
    new TestSets {
      val s = filter(s0, fGreaterThan1) // s0 = {}; s = {}
      assert(!contains(s0, 1), "Filter 1")
      assert(!contains(s0, 2), "Filter 2")
      assert(!contains(s0, 3), "Filter 3")
    }
  }

  test("forAll - ALL numbers from 10 to 100 are all positive") {
    new TestSets {
      assert(forall(set10To100, fPositive))
    }
  }

  test("forAll - NOT ALL numbers from 10 to 100 are greater than 10") {
    new TestSets {
    assert(!forall(set10To100, fGreaterThan10))
    }
  }

  test("forAll - NONE negative number is greater than 10") {
    new TestSets {
      assert(!forall(setNegative, fGreaterThan10))
    }
  }

  test("forAll - empty set") {
    new TestSets {
      assert(!forall(s0, fAll))
    }
  }

  test("exists - numbers divisible by 5 are also divisible by 2") {
    new TestSets {
      assert(exists(setDiv5, fDivisibleBy2))
    }
  }

  test("exists - one negative number is greater than -2") {
    new TestSets {
      assert(exists(setNegative, fGreaterThanNeg2))
    }
  }

  test("exists - NONE negative number is greater than 10") {
    new TestSets {
      assert(!exists(setNegative, fGreaterThan10))
    }
  }

  test("exists - empty set") {
    new TestSets {
      assert(!exists(s0, fAll))
    }
  }

  test("map - numbers divisible by 5 increment by 3") {
    new TestSets {
      val s = map(setDiv5, fAdd3)
      assert(!contains(s, 5), "Map Element 5")
      assert(contains(s, 8), "Map Element 8")
      assert(!contains(s, 1), "Map Element 1")
    }
  }

  test("map - singleton 1 increment by 3") {
    new TestSets {
      val s = map(s1, fAdd3)
      assert(!contains(s, 1), "Map Element 1")
      assert(contains(s, 4), "Map Element 4")
      assert(!contains(s, -2), "Map Element -2")
    }
  }

  test("map - empty set") {
    new TestSets {
      val s = map(s0, fAdd3)
      assert(!exists(s, fAll))
      assert(!contains(s, 1), "Map Element 1")
      assert(!contains(s, 3), "Map Element 3")
    }
  }
}
