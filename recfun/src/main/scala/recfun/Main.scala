package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    val max = 12
    for (row <- 0 to max) {
      for (col <- 0 to row) {
        printf("%5d", pascal(col, row))
      }
      println()
    }
  }

  /**
   * Exercise 1.
   *
   * The numbers at the edge of the triangle are all 1, and each number inside the triangle is the sum of the two
   * numbers above it. Write a function that computes the elements of Pascal’s triangle by means of a recursive process.
   *
   * Do this exercise by implementing the pascal function in Main.scala, which takes a column c and a row r,
   * counting from 0 and returns the number at that spot in the triangle.
   * For example, pascal(0,2)=1, pascal(1,2)=2 and pascal(1,3)=3.
   */
  def pascal(c: Int, r: Int): Int = {
    if (c > r) -1
    else {
      if (c == 0 || c == r) 1
      else pascal(c - 1, r - 1) + pascal(c, r - 1)
    }
  }

  /**
   * Exercise 2.
   *
   * Write a recursive function which verifies the balancing of parentheses in a string, which we represent as a
   * List[Char] not a String.
   *
   * For example, the function should return true for the following strings:
   * - (if (zero? x) max (/ 1 x))
   * - I told him (that it’s not (yet) done). (But he wasn’t listening)
   *
   * The function should return false for the following strings:
   * - :-)
   * - ())(
   *
   * The last example shows that it’s not enough to verify that a string contains the same number of
   * opening and closing parentheses.
   */
  def balance(chars: List[Char]): Boolean = {

    def balanceIter(chars: List[Char], parenthCount: Int): Boolean = {
      if (parenthCount < 0) false
      else if (chars.isEmpty) parenthCount == 0
      else {
        val head = chars.head
        if (head == '(') balanceIter(chars.tail, parenthCount + 1)
        else if (head == ')') balanceIter(chars.tail, parenthCount - 1)
        else balanceIter(chars.tail, parenthCount)
      }
    }

    balanceIter(chars, 0)
  }

  /**
   * Exercise 3.
   *
   * Write a recursive function that counts how many different ways you can make change for an amount, given a list of
   * coin denominations.
   * For example, there are 3 ways to give change for 4 if you have coins with denomiation 1 and 2: 1+1+1+1, 1+1+2, 2+2.
   */
  def countChange(money: Int, coins: List[Int]): Int = {

    def countUniqueSums(max : Int, coins : List[Int], current : Int): Int = {
      if(current > max) 0
      else if(current == max) 1
      else {
        if(coins.isEmpty) 0
        else {
          countUniqueSums(max, coins, current + coins.head) + 
          countUniqueSums(max, coins.tail, current)
        }
      }
    }

    if (money <= 0) 0
    else if (coins.isEmpty) 0
    else {
      val result = countUniqueSums(money, coins, 0)
      println("result = " + result)
      result
    }
  }
}
