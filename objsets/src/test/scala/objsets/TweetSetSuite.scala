package objsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TweetSetSuite extends FunSuite {
  trait TestSets {
    val a = new Tweet("a", "a body", 20)
    val b = new Tweet("b", "b body", 20)
    val c = new Tweet("c", "c body", 7)
    val d = new Tweet("d", "d body", 9)

    val set0 = new Empty
    val setA = set0.incl(a)
    val setAB = setA.incl(b)
    val setABC = setAB.incl(c)
    val setABD = setAB.incl(d)
    val setABCD = setABC.incl(d)
    val setCD = set0.incl(c).incl(d)
    val setDCB = set0.incl(d).incl(c).incl(b)
    val setCCCC = set0.incl(c).incl(c).incl(c).incl(c) // {c}

//    println("setABCD = " + setABCD)
//    println(setABCD.descendingByRetweet.toString())
  }

  def asSet(tweets: TweetSet): Set[Tweet] = {
    var res = Set[Tweet]()
    tweets.foreach(res += _)
    res
  }

  def size(set: TweetSet): Int = asSet(set).size

  test("filter: on empty set") {
    new TestSets {
      assert(size(set0.filter(tw => tw.user == "a")) === 0)
    }
  }

  test("filter: a on setABCD") {
    new TestSets {
      println("setABCD = " + setABCD.toString())
      assert(size(setABCD.filter(tw => tw.user == "a")) === 1) // {a}
    }
  }

  test("filter: 20 on setABCD") {
    new TestSets {
      assert(size(setABCD.filter(tw => tw.retweets == 20)) === 2) // {a,b}
    }
  }

  test("union: setABC and setABD") {
    new TestSets {
      assert(size(setABC.union(setABD)) === 4) // {a,b,c,d}
    }
  }

  test("union: with itself") {
    new TestSets {
      assert(size(setAB.union(setAB)) === 2) // {a,b}
    }
  }

  test("union: with empty set on the right") {
    new TestSets {
      assert(size(setABCD.union(set0)) === 4) // {a,b,c,d}
    }
  }

  test("union: with empty set on the left") {
    new TestSets {
      assert(size(set0.union(setABCD)) === 4) // {a,b,c,d}
    }
  }

  test("union: both empty") {
    new TestSets {
      assert(size(set0.union(set0)) === 0) // {}
    }
  }

  test("mostRetweeted: setABCD") {
    new TestSets {
      val mostRetweeted = setABCD.mostRetweeted
      assert(mostRetweeted.retweets === 20) // {}
    }
  }

  test("mostRetweeted: setCD") {
    new TestSets {
      val mostRetweeted = setCD.mostRetweeted
      assert(mostRetweeted.retweets === 9) // {}
    }
  }

  test("mostRetweeted: empty set") {
    new TestSets {
      intercept[NoSuchElementException] {
        set0.mostRetweeted
      }
    }
  }

  test("descending: setABCD") {
    new TestSets {
      val trends = setABCD.descendingByRetweet
      println("trends = " + trends)
      assert(!trends.isEmpty)
      assert(trends.head.user == "a" || trends.head.user == "b")
    }
  }

  test("descending: setDCB") {
    new TestSets {
      val trends = setDCB.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user == "b" && trends.tail.head.user == "d")
    }
  }

  test("descending: setCCCC") {
    new TestSets {
      val trends = setCCCC.descendingByRetweet
      println("trends = " + trends)
      assert(!trends.isEmpty)
      assert(trends.head.user == "c" && trends.tail.isEmpty)
    }
  }

  test("descending: empty set") {
    new TestSets {
      val trends = set0.descendingByRetweet
      assert(trends.isEmpty)
    }
  }

  test("google vs apple: allTweets") {
    assert(!GoogleVsApple.allTweets.isEmpty)
  }

  test("google vs apple: appleTweets") {
    assert(!GoogleVsApple.appleTweets.isEmpty)
  }

  test("google vs apple: googleTweets") {
    assert(!GoogleVsApple.googleTweets.isEmpty)
  }

  test("google vs apple: trending") {
    assert(GoogleVsApple.trending.head.retweets === 321)
  }
}
