package forcomp

object anagram {
  println("Welcome to the Scala worksheet - ANAGRAM")
                                                  //> Welcome to the Scala worksheet - ANAGRAM

  "Testowy".toList                                //> res0: List[Char] = List(T, e, s, t, o, w, y)

  "Testowy".toList.map(_.toLower)                 //> res1: List[Char] = List(t, e, s, t, o, w, y)

  "Testowy".toList.groupBy((c: Char) => c)        //> res2: scala.collection.immutable.Map[Char,List[Char]] = Map(e -> List(e), s 
                                                  //| -> List(s), y -> List(y), T -> List(T), t -> List(t), w -> List(w), o -> Lis
                                                  //| t(o))

  val mapCharListOfChar = "Testowy".toList.map(_.toLower).groupBy((c: Char) => c)
                                                  //> mapCharListOfChar  : scala.collection.immutable.Map[Char,List[Char]] = Map(e
                                                  //|  -> List(e), s -> List(s), y -> List(y), t -> List(t, t), w -> List(w), o ->
                                                  //|  List(o))

  mapCharListOfChar.map(elem => (elem._1, elem._2.length))
                                                  //> res3: scala.collection.immutable.Map[Char,Int] = Map(e -> 1, s -> 1, y -> 1,
                                                  //|  t -> 2, w -> 1, o -> 1)

  mapCharListOfChar.map(elem => (elem._1, elem._2.length)).toList
                                                  //> res4: List[(Char, Int)] = List((e,1), (s,1), (y,1), (t,2), (w,1), (o,1))

  mapCharListOfChar.map(elem => (elem._1, elem._2.length)).toList.sorted
                                                  //> res5: List[(Char, Int)] = List((e,1), (o,1), (s,1), (t,2), (w,1), (y,1))

  mapCharListOfChar.map(elem => (elem._1, elem._2.length)).map { case (k, v) => (k, v) }
                                                  //> res6: scala.collection.immutable.Map[Char,Int] = Map(e -> 1, s -> 1, y -> 1,
                                                  //|  t -> 2, w -> 1, o -> 1)

  val words = List("Test", "my", "scala", "is", "getting", "better")
                                                  //> words  : List[String] = List(Test, my, scala, is, getting, better)
  words.toString                                  //> res7: String = List(Test, my, scala, is, getting, better)
  words.flatten                                   //> res8: List[Char] = List(T, e, s, t, m, y, s, c, a, l, a, i, s, g, e, t, t, i
                                                  //| , n, g, b, e, t, t, e, r)

  words.flatten.mkString("")                      //> res9: String = Testmyscalaisgettingbetter

  //Anagrams.dictionary

  //val dictWordOccurences = Anagrams.dictionary.map(word => (Anagrams.wordOccurrences(word), word))

  //dictWordOccurences.groupBy((pair: (Anagrams.Occurrences, Anagrams.Word)) => pair._1)

  val simpleDict = List("ate", "eat", "tea", "car", "arc")
                                                  //> simpleDict  : List[String] = List(ate, eat, tea, car, arc)

  val simpleDictWordOccurences1 = simpleDict.map((Anagrams.wordOccurrences(_), _))
                                                  //> simpleDictWordOccurences1  : List[(forcomp.Anagrams.Word => forcomp.Anagram
                                                  //| s.Occurrences, String)] = List((<function1>,ate), (<function1>,eat), (<func
                                                  //| tion1>,tea), (<function1>,car), (<function1>,arc))
  val simpleDictWordOccurences = simpleDict.map(word => (Anagrams.wordOccurrences(word), word))
                                                  //> simpleDictWordOccurences  : List[(forcomp.Anagrams.Occurrences, String)] = 
                                                  //| List((List((a,1), (e,1), (t,1)),ate), (List((a,1), (e,1), (t,1)),eat), (Lis
                                                  //| t((a,1), (e,1), (t,1)),tea), (List((a,1), (c,1), (r,1)),car), (List((a,1), 
                                                  //| (c,1), (r,1)),arc))
  simpleDictWordOccurences.groupBy(_._1)          //> res10: scala.collection.immutable.Map[forcomp.Anagrams.Occurrences,List[(fo
                                                  //| rcomp.Anagrams.Occurrences, String)]] = Map(List((a,1), (c,1), (r,1)) -> Li
                                                  //| st((List((a,1), (c,1), (r,1)),car), (List((a,1), (c,1), (r,1)),arc)), List(
                                                  //| (a,1), (e,1), (t,1)) -> List((List((a,1), (e,1), (t,1)),ate), (List((a,1), 
                                                  //| (e,1), (t,1)),eat), (List((a,1), (e,1), (t,1)),tea)))

  simpleDictWordOccurences.groupBy(_._1).map { case (k, v) => (k, v.map(_._2)) }
                                                  //> res11: scala.collection.immutable.Map[forcomp.Anagrams.Occurrences,List[Str
                                                  //| ing]] = Map(List((a,1), (c,1), (r,1)) -> List(car, arc), List((a,1), (e,1),
                                                  //|  (t,1)) -> List(ate, eat, tea))
  //Anagrams.dictionary.map(word => (Anagrams.wordOccurrences(word), word.toList))

  val occurrancesKLM = ('k', 2) :: List(('l', 2), ('m', 1))
                                                  //> occurrancesKLM  : List[(Char, Int)] = List((k,2), (l,2), (m,1))
  val occurrancesAB = List(('a', 2), ('b', 2))    //> occurrancesAB  : List[(Char, Int)] = List((a,2), (b,2))

  val singleOcc = ('a', 3)                        //> singleOcc  : (Char, Int) = (a,3)

  (for (num <- 1 to singleOcc._2) yield (singleOcc._1, num)).toList
                                                  //> res12: List[(Char, Int)] = List((a,1), (a,2), (a,3))

  val listPerCharKLM = for (singleOcc <- occurrancesKLM) yield (for (num <- 1 to singleOcc._2) yield (singleOcc._1, num)).toList
                                                  //> listPerCharKLM  : List[List[(Char, Int)]] = List(List((k,1), (k,2)), List((
                                                  //| l,1), (l,2)), List((m,1)))

  val listPerCharAB = for (singleOcc <- occurrancesAB) yield (for (num <- 1 to singleOcc._2) yield (singleOcc._1, num)).toList
                                                  //> listPerCharAB  : List[List[(Char, Int)]] = List(List((a,1), (a,2)), List((b
                                                  //| ,1), (b,2)))

  listPerCharAB.flatten                           //> res13: List[(Char, Int)] = List((a,1), (a,2), (b,1), (b,2))

}